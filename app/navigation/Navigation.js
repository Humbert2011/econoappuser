import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import {Icon} from 'react-native-elements';

import HomeStack from "./HomeStack";
import PromocionesStack from './PromocionesStack';
import ComprarStack from './ComprarStack';
import Cuenta from "./CuentaStack";
const Tab = createBottomTabNavigator();

export default function Navigation() {
  return (
    <NavigationContainer>
      <Tab.Navigator 
        initialRouteName="restaurantes"
        screenOptions={({route})=>({
          tabBarIcon: ({color}) => iconos(route,color),
          tabBarActiveTintColor: '#FF6719',
          tabBarInactiveTintColor: 'black',
          headerShown : false
        })}
      >
          <Tab.Screen name="homeStack" options={{title:"Home"}} component={HomeStack}/>
          <Tab.Screen name="comprarStack" options={{title:"Comprar"}}  component={ComprarStack} />
          <Tab.Screen name="promocionesStack" options={{title:"Promociones"}}  component={PromocionesStack} />
          <Tab.Screen name="cuentaStack" options={{title:"Cuenta"}}  component={Cuenta} />
      </Tab.Navigator>
    </NavigationContainer>
  );
}

const iconos = (route,color) => {
  let iconNombre;
  switch (route.name){
    case "homeStack":
        iconNombre = "home"
      break;
    case "categoriasStack":
      iconNombre = "th-large"
      break;
      case "comprarStack":
      iconNombre = "usd"
      break;
    case "favoritosStack":
        iconNombre = "heart"
      break;
    case "promocionesStack":
        iconNombre = "tags"
      break;
    case "cuentaStack":
        iconNombre = "user"
      break;
  }
  
  return(
    <Icon type="font-awesome" name = {iconNombre} size={22} color={color} />
  )
}