import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import Login from "../screens/cuenta/Login";
import IngresaNumero from '../screens/cuenta/registro/IngresaNumero';
import CuentaEspera from '../screens/cuenta/CuentaEspera';
import RecuperarPassword from '../screens/cuenta/recuperarPassword/RecuperarPassword';
import ValidarCodigo from '../screens/cuenta/registro/ValidarCodigo';
import Registro from '../screens/cuenta/registro/Registro';
import Perfil from '../screens/cuenta/opcionesUser/Perfil';
import ListarDirecciones from '../screens/direcciones/ListarDirecciones';
import AddDireccion from '../screens/direcciones/AddDireccion';
import MetodoPago from '../screens/metodoPago/MetodoPago';
import TerminosCondiciones from '../screens/cuenta/opcionesUser/TerminosCondiciones';
import Contactanos from '../screens/cuenta/opcionesUser/Contactanos';
import MisPedidos from '../screens/cuenta/opcionesUser/MisPedidos';
import DetallePedidos from '../screens/cuenta/opcionesUser/DetallePedidos';
import HeaderCubierta from '../components/header/HeaderCubierta';


const Stack = createStackNavigator();

function CuentaStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen name="espera" component={CuentaEspera}
        options={{
          title: "Cuenta",
          headerStyle: {
            backgroundColor: "rgba(61,92,164,1)",
            height: 75
          },
          headerTintColor: '#fff',

        }}
      />
      <Stack.Screen name="login" component={Login}
        options={{
          title: "Atrás",
          headerStyle: {
            backgroundColor: "#fff",
            height: 75
          },
          headerTintColor: "rgba(0,0,0,1)",

        }}
      />
      {/* Opciones User */}
      <Stack.Screen name="perfil" component={Perfil}
        options={{
          title: "Perfil",
          headerStyle: {
            backgroundColor: "rgba(61,92,164,1)",
            height: 75
          },
          headerTintColor: "#fff",

        }}
      />
      <Stack.Screen name="buscar" component={HeaderCubierta}
        options={({ route }) => (
          {
            headerTitle: props => <Text style={{ color: "#fff", fontSize: 21, marginLeft: -20 }} >Carrito</Text>,
            headerStyle: {
              backgroundColor: "rgba(61,92,164,1)",
              height: 75
            },
            title: "Buscador",
            headerTintColor: '#fff',
            headerBackTitleVisible: false,
            headerLeft: false,
            headerShown: false,
          }
        )}
      />

      {/* Mis pedidos */}
      <Stack.Screen name="misPedidos" component={MisPedidos}
        options={{
          title: "Mis Pedidos",
          headerStyle: {
            backgroundColor: "rgba(61,92,164,1)",
            height: 75
          },
          headerTintColor: "#fff",
        }}
      />

      {/*Detalle de pedido*/}
      <Stack.Screen name="detallePedidos" component={DetallePedidos}
        options={{
          title: "Detalle de pedido",
          headerStyle: {
            backgroundColor: "rgba(61,92,164,1)",
            height: 75
          },
          headerTintColor: "#fff",
        }}
      />

      {/* direcciones */}
      <Stack.Screen name="direcciones" component={ListarDirecciones}
        options={{
          title: "",
          headerStyle: {
            backgroundColor: "#fff",
            height: 75
          },
          // headerTintColor: "#fff",

        }}
      />
      <Stack.Screen name="adddireccion" component={AddDireccion}
        options={{
          title: "Confirma tu dirección",
          headerStyle: {
            backgroundColor: "#fff",
            height: 109
          },
          // headerTintColor: "#fff",

        }}
      />
      {/* Metodo pago */}
      <Stack.Screen name="metodoPago" component={MetodoPago}
        options={{
          title: "Método de Pago",
          headerStyle: {
            backgroundColor: "rgba(61,92,164,1)",
            height: 75
          },
          headerTintColor: "#fff",

        }}
      />
      {/* registro */}
      <Stack.Screen name="ingresaNumero" component={IngresaNumero}
        options={{
          title: "Atrás",
          headerStyle: {
            backgroundColor: "#fff",
            height: 75
          },
          headerTintColor: 'rgba(0,0,0,1)',

        }}
      />
      <Stack.Screen name="validarCodigo" component={ValidarCodigo} options={{ title: "Atrás" }} />
      <Stack.Screen name="registro" component={Registro} options={{ title: "Atrás" }} />
      {/* recuperarPassword */}
      <Stack.Screen name="recuperarPassword" component={RecuperarPassword} options={{ title: "Recuperar Password" }} />


      {/* Términos y Condiciones */}
      <Stack.Screen name="terminosCondiciones" component={TerminosCondiciones}
        options={{
          title: "Términos y Condiciones",
          headerStyle: {
            backgroundColor: "rgba(61,92,164,1)",
            height: 75
          },
          headerTintColor: "#fff",
        }}
      />

      {/* Contáctanos */}
      <Stack.Screen name="contactanos" component={Contactanos}
        options={{
          title: "Contáctanos",
          headerStyle: {
            backgroundColor: "rgba(61,92,164,1)",
            height: 75
          },
          headerTintColor: "#fff",
        }}
      />
    </Stack.Navigator>
  );
}

export default CuentaStack;