import * as React from 'react';
import {Text} from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';

import Categorias from "../screens/categorias/Categorias";
import ListProductsCategoria from '../screens/categorias/ListProductsCategoria';
import ListForSubcategorias from '../screens/categorias/ListForSubcategorias';
import HeaderInicio from '../components/header/HeaderInicio';
import HeaderBasico from '../components/header/HeaderBasico'
import DetalleProd from '../screens/generic/DetalleProd';
import HeaderTitle from '../components/header/HeaderTitle';
import Cart from '../screens/cart/Cart';
import HeaderCubierta from '../components/header/HeaderCubierta';


const Stack = createStackNavigator();

function CategoriasStack() {
  return (
      <Stack.Navigator>
        <Stack.Screen name="categorias" component={Categorias}
                        options={
                            { 
                            headerTitle: props => <HeaderInicio {...props} />,
                            headerStyle:{
                                backgroundColor: "rgba(61,92,164,1)",
                                height:122
                            },
                            headerTintColor: '#fff',
                            }}
        />
        <Stack.Screen name = "listproductscategoria" component ={ListProductsCategoria}
        options={({ route }) => (
            { 
              headerTitle : props => <HeaderTitle route = {route} />,
              headerStyle:{
                backgroundColor: "rgba(61,92,164,1)",
                height:122
              },
              headerTintColor : '#fff',
              headerBackTitleVisible : false
            }
          )}
        />
        <Stack.Screen name="prodforsucategorias" component={ListForSubcategorias}
                         options={({ route }) => (
                          { 
                            headerTitle : props => <HeaderTitle route = {route} />,
                            headerStyle:{
                              backgroundColor: "rgba(61,92,164,1)",
                              height:122
                            },
                            headerTintColor : '#fff',
                            headerBackTitleVisible : false
                          }
                        )}
        />
        <Stack.Screen name="detalleproducto" component={DetalleProd}
                      options={
                      { 
                      headerTitle: props => <HeaderBasico {...props} />,
                      headerStyle:{
                          backgroundColor: "rgba(61,92,164,1)",
                          height:100
                      },
                      headerTintColor: '#fff',
                      headerBackTitleVisible : false
                      }}
        />
        <Stack.Screen name="buscar" component={HeaderCubierta}
        options={({ route }) => (
          {
            headerTitle: props => <Text style={{ color: "#fff", fontSize: 21, marginLeft: -20 }} >Carrito</Text>,
            headerStyle: {
              backgroundColor: "rgba(61,92,164,1)",
              height: 75
            },
            title: "Buscador",
            headerTintColor: '#fff',
            headerBackTitleVisible: false,
            headerLeft: false,
            headerShown: false,
          }
        )}
      />
        <Stack.Screen name="cart" component={Cart}
                         options={({ route }) => (
                          { 
                            headerTitle : props => <Text style={{color:"#fff",fontSize:21,marginLeft:-20}} >Carrito</Text> ,
                            headerStyle:{
                              backgroundColor: "rgba(61,92,164,1)",
                              height:100
                            },
                            headerTintColor : '#fff',
                            headerBackTitleVisible : false
                          }
                        )}
        />
      </Stack.Navigator>
  );
}

export default CategoriasStack;