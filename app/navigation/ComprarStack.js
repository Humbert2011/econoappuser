import * as React from 'react';
import {Text} from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';

import ListProductsCategoria from '../screens/categorias/ListProductsCategoria';
import ListForSubcategorias from '../screens/categorias/ListForSubcategorias';
import HeaderBasico from '../components/header/HeaderBasico'
import DetalleProd from '../screens/generic/DetalleProd';
import HeaderTitle from '../components/header/HeaderTitle';
import Cart from '../screens/cart/Cart';
import AddDireccion from '../screens/direcciones/AddDireccion';
import ListarDireccionesCart from '../screens/direcciones/ListarDireccionesCart';
import HeaderCubierta from '../components/header/HeaderCubierta';



const Stack = createStackNavigator();

function ComprarStack() {
  return (
      <Stack.Navigator>
        <Stack.Screen name = "listproductscategoria" component ={ListProductsCategoria}
        options={({ route }) => (
            { 
              headerTitle : props => <HeaderTitle route = {route} />,
              headerStyle:{
                backgroundColor: "rgba(61,92,164,1)",
                height:100
              },
              headerTintColor : '#fff',
              headerBackTitleVisible : false
            }
          )}
        />
        <Stack.Screen name="prodforsucategorias" component={ListForSubcategorias}
                         options={({ route }) => (
                          { 
                            headerTitle : props => <HeaderTitle route = {route} />,
                            headerStyle:{
                              backgroundColor: "rgba(61,92,164,1)",
                              height:100
                            },
                            headerTintColor : '#fff',
                            headerBackTitleVisible : false
                          }
                        )}
        />
        <Stack.Screen name="buscar" component={HeaderCubierta}
        options={({ route }) => (
          {
            headerTitle: props => <Text style={{ color: "#fff", fontSize: 21, marginLeft: -20 }} >Carrito</Text>,
            headerStyle: {
              backgroundColor: "rgba(61,92,164,1)",
              height: 75
            },
            title: "Buscador",
            headerTintColor: '#fff',
            headerBackTitleVisible: false,
            headerLeft: false,
            headerShown: false,
          }
        )}
      />
        <Stack.Screen name="detalleproducto" component={DetalleProd}
                      options={
                      { 
                      headerTitle: props => <HeaderBasico {...props} />,
                      headerStyle:{
                          backgroundColor: "rgba(61,92,164,1)",
                          height:100
                      },
                      headerTintColor: '#fff',
                      headerBackTitleVisible : false
                      }}
        />
        <Stack.Screen name="cart" component={Cart}
                         options={({ route }) => (
                          { 
                            headerTitle : props => <Text style={{color:"#fff",fontSize:21,marginLeft:-20}} >Carrito</Text> ,
                            headerStyle:{
                              backgroundColor: "rgba(61,92,164,1)",
                              height:75
                            },
                            title:"Carrito",
                            headerTintColor : '#fff',
                            headerBackTitleVisible : false
                          }
                        )}
        />
        {/* direccion */}
        <Stack.Screen name="adddireccion" component={AddDireccion} 
                options={ {
                  title:"Agrega tu dirección",
                  headerStyle:{
                    backgroundColor: "#fff",
                    height:75
                }
               }}
        />
        <Stack.Screen name="listardireccionesCart" component={ListarDireccionesCart} 
                options={ {
                  title:"",
                  headerStyle:{
                    backgroundColor: "#fff",
                    height:75
                },
               }}
        />
      </Stack.Navigator>
  );
}

export default ComprarStack;