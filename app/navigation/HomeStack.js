import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { Text } from 'react-native';
import Home from "../screens/home/Home";

import HeaderInicio from '../components/header/HeaderInicio';
import HeaderBasico from '../components/header/HeaderBasico';

import DetalleProd from '../screens/generic/DetalleProd';


import Cart from '../screens/cart/Cart';
import HeaderCubierta from '../components/header/HeaderCubierta';
import AddDireccion from '../screens/direcciones/AddDireccion';
import ListarDireccionesCart from '../screens/direcciones/ListarDireccionesCart';


const Stack = createStackNavigator();

function HomeStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen name="home" component={Home}
        // options={{title:"Hola"}}
        options={
          {
            headerTitle: props => <HeaderInicio {...props} />,
            headerStyle: {
              backgroundColor: "rgba(61,92,164,1)",
              height: 122
            },

          }
        }
      />
      <Stack.Screen name="detalleproducto" component={DetalleProd}
        options={
          { 
          headerTitle: props => <HeaderBasico {...props} />,
          headerStyle:{
              backgroundColor: "rgba(61,92,164,1)",
              height:100
          },
          headerTintColor: '#fff',
          headerBackTitleVisible : false
          }}
      />
      <Stack.Screen name="cart" component={Cart}
        options={({ route }) => (
          {
            headerTitle: props => <Text style={{ color: "#fff", fontSize: 21, marginLeft: -20 }} >Carrito</Text>,
            headerStyle: {
              backgroundColor: "rgba(61,92,164,1)",
              height: 75
            },
            title: "Carrito",
            headerTintColor: '#fff',
            headerBackTitleVisible: false
          }
        )}
      />
      <Stack.Screen name="buscar" component={HeaderCubierta}
        options={({ route }) => (
          {
            headerTitle: props => <Text style={{ color: "#fff", fontSize: 21, marginLeft: -20 }} >Carrito</Text>,
            headerStyle: {
              backgroundColor: "rgba(61,92,164,1)",
              height: 75
            },
            title: "Buscador",
            headerTintColor: '#fff',
            headerBackTitleVisible: false,
            headerLeft: false,
            headerShown: false,
          }
        )}
      />
      <Stack.Screen name="adddireccion" component={AddDireccion}
        options={{
          title: "Agrega tu dirección",
          headerStyle: {
            backgroundColor: "#fff",
            height: 75
          },
          // headerTintColor: "#fff",

        }}
      />
      <Stack.Screen name="listardireccionesCart" component={ListarDireccionesCart}
        options={{
          title: "",
          headerStyle: {
            backgroundColor: "#fff",
            height: 75
          },
          // headerTintColor: "#fff",

        }}
      />
    </Stack.Navigator>
  );
}

export default HomeStack;