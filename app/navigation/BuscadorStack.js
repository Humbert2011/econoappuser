import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import Restaurantes from "../screens/home/Home";

const Stack = createStackNavigator();

function BuscadorStack() {
  return (
      <Stack.Navigator>
        <Stack.Screen name="home" component={Restaurantes} options={{title:"Home"}} />
      </Stack.Navigator>
  );
}

export default BuscadorStack;