import React from "react";
import { ActivityIndicator, StyleSheet, Text, View } from "react-native";
import {Overlay } from 'react-native-elements';


export default function Loader(props){
    const {isVisible,text} = props;

    return(
        <Overlay
            isVisible={isVisible}
            windowBackgroundColor = "rgba(0,0,0,0.5)"
            overlayBackgroundColor = "transparent"
            overlayStyle = {stilos.overlay}
        >
            <View style={stilos.vista}>
                <ActivityIndicator size="large" color="#3D5CA4" />
                {/* if corto de solo return true */}
                {text && <Text>{text}</Text>}
            </View>
        </Overlay>
    );
}

const stilos = StyleSheet.create({
    overlay:{
        height:100,
        width:200,
        // backgroundColor:"#fff",
        // borderColor:"blue",
        // borderWidth: 2,
        borderRadius:10
    },
    vista:{
        flex:1,
        alignItems: "center",
        justifyContent: "center"
    },
    text:{
        color:"blue",
        textTransform:"uppercase",
        marginTop: 10
    }
});