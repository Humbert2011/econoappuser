import React from 'react'
import { View, Text } from 'react-native'
import { LinearProgress } from 'react-native-elements';

const LoaderLineal = () => {
    return (
        <View style={{ alignItems:'center',justifyContent:'center'}}>
            <LinearProgress style={{width:250, margin:20}} color="#FF6719" />
        </View>
    )
}

export default LoaderLineal
