import { StyleSheet, Image, View, TouchableOpacity, FlatList, Text, ActivityIndicator } from 'react-native'
import { ListItem } from 'react-native-elements';
import { useNavigation } from '@react-navigation/native';
import React from 'react'

import img from '../../../assets/s.jpg'

const ListaPedidos = ({ pedidos, morePedidos, isLoading }) => {
  return (
    <FlatList
      data={pedidos}
      renderItem={(pedido) => (<Pedido pedido={pedido} />)}
      keyExtractor={(item, index) => index.toString()}
      onEndReachedThreshold={0.5}
      onEndReached={morePedidos}
      ListFooterComponent={<FooterList isLoading={isLoading} />}
    />
  )
}

const FooterList = (props) => {
  const { isLoading } = props;
  if (isLoading) {
    return (
      <View style={styles.loaderPedidos}>
        <ActivityIndicator size="large" />
      </View>
    );
  } else {
    return (
      <View style={styles.notFoundPedidos}>
        {/* <Text>No quedan pedidos por cargar</Text> */}
      </View>
    );
  }
}

const Pedido = ({ pedido }) => {
  const navegacion = useNavigation();
  // console.log(pedido)
  let l = pedido.item;
  return (
    <ListItem bottomDivider onPress={() => navegacion.navigate('detallePedidos', { idpedido: l.idpedido })}>
      <Image source={img} style={styles.logoView} />
      <ListItem.Content >
        <ListItem.Title style={styles.contentView} allowFontScaling={false}>Econosuper </ListItem.Title>
        <ListItem.Subtitle style={styles.contentView} allowFontScaling={false}>Suc.Huauchinango </ListItem.Subtitle>

        <ListItem.Title style={styles.title} allowFontScaling={false}>
          No.de pedido: {l.idpedido}
        </ListItem.Title>
        <ListItem.Subtitle style={styles.subtitle} allowFontScaling={false}>
          {l.nombrestatus}
        </ListItem.Subtitle>
      </ListItem.Content>
      <TouchableOpacity style={{ marginRight: 10 }} onPress={() => navegacion.navigate('detallePedidos', { idpedido: l.idpedido })}>
        <ListItem.Chevron color='#FC6915' size={40} />
      </TouchableOpacity>
    </ListItem>
  )

}

export default ListaPedidos

const styles = StyleSheet.create({
  title: {
    color: 'gray',
    fontWeight: 'bold',
    fontSize: 14.85,
    lineHeight: 33
  },
  subtitle: {
    color: 'black',
    fontSize: 14.85,
    fontWeight: 'bold'
  },
  logoView: {
    height: 60,
    width: 60,
    borderRadius: 50,
    backgroundColor: 'gray',
  },
  contentView: {
    color: 'black',
    fontSize: 18.85,
    fontWeight: 'bold'
  },
  loaderPedidos: {
    marginTop: 10,
    marginBottom: 10,
    alignItems: "center",
  },
  notFoundPedidos: {
    marginTop: 10,
    marginBottom: 20,
    alignItems: "center",
  },
})