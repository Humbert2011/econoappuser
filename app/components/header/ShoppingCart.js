import React,{useState} from 'react'
import {View } from 'react-native'
import {Image} from 'react-native-elements';
import {useNavigation } from '@react-navigation/native';
// import Cart from '../Cart'
// import {getCart} from '../../utils/cartStorage'
const ShoppingCart = () => {
    const navegacion = useNavigation();
    const [isVisible, setIsVisible] = useState(false)
    const [listElementsCart, setlistElementsCart] = useState(null);

    // const showCart = async () => {
    //     let db = getCart();
    //     await db.transaction((tx) => {
    //         tx.executeSql("select * from items", [], (_, { rows: { _array } }) =>
    //             setlistElementsCart(_array)
    //         );
    //     })
    //     setIsVisible(!isVisible);
    // }

    return (
        <View>
            <Image
                resizeMode = "stretch"
                source = {require("../../../assets/shopping-cart-icon.png")}
                style = {{width: 26, height: 24, marginRight:2 }}
                onPress = {()=>navegacion.navigate('cart')}
            />
            {/* <Cart isVisible = {isVisible} setIsVisible = {setIsVisible} listElementsCart = {listElementsCart}/> */}
        </View>
    )
}

export default ShoppingCart;