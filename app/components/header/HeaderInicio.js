import React, {useState} from 'react'
import { StyleSheet, View, TouchableOpacity, Text } from 'react-native'
import { Image, Overlay, Icon} from 'react-native-elements';
import HeaderCubierta from './HeaderCubierta';
import SearchBarra from './SearchBarra'
import ShoppingCart from './ShoppingCart'



const Header = () => {

const [visible, setVisible] = useState(false)

const toggleOverlay = () =>{
       setVisible(!visible)
}

    return (
        <View>
            <View style={{ marginBottom: -2 }} >
                <View style={{ flexDirection: 'row', marginLeft: 5, marginRight: 5 }}>
                    <View style={{ width: '50%', height:'100%', alignItems: "flex-start", top:'2.7%', left:'14%'}} >
                        <Image
                            resizeMode="cover"
                            source={
                                require("../../../assets/logoBlanco.png")
                            }
                            style={styles.imglogo}
                        />
                    </View>
                    <View style={{ width: '50%', alignItems: "flex-end", top:'4.2%', left:'10%' }} >
                        <ShoppingCart />
                    </View>
                </View>
            </View>
            <View style={{height:'80%', top:'6%'}}>
               <SearchBarra />
            </View>
        </View>
    )
}

export default Header

const styles = StyleSheet.create({
    imglogo: {
        height: 36,
        width: 130
    },
    btnSearch: {
        backgroundColor: 'white',
        //borderColor: 'white',
        width: '100%',
        height: '55%',
        padding: '1%',
        borderRadius: 25,
        marginTop: 10,
        //top: '18%',
        alignItems: 'center',
        justifyContent: 'center'
    },

})
