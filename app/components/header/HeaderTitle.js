import React from 'react'
import { StyleSheet, Text, View } from 'react-native';

import ShoppingCart from '../../components/header/ShoppingCart'

const HeaderTitle = ({route}) => {
    // const {subcategoria} = route.params;
    const  nombre = 'Frutas y Verduras';
    let subcategoria = false;
    let title = nombre;
    subcategoria && (title = `${title} - ${subcategoria}`);
    return (
        <View style = {{flexDirection: "row"}}>
            <View style = {{width:"80%"}} >
                <Text style={{color:"#fff",fontSize:21}}>{title}</Text>
            </View>
            <View style = {{alignItems:"flex-end",width:"20%"}} >
                <ShoppingCart/>
            </View>
        </View>
    )
}

export default HeaderTitle

const styles = StyleSheet.create({})
