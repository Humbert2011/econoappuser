import React from 'react'
import { StyleSheet, View } from 'react-native'
import { TouchableOpacity} from 'react-native'
import { Text } from 'react-native'
import {useNavigation } from '@react-navigation/native';

import ShoppingCart from './ShoppingCart'

const HeaderBasico = () => {

    const openSearch = (e) => {
        console.log("Abrir busqueda");
    }
    const navegacion = useNavigation();
    return (
        <View style={{ width: "100%", height: "100%", alignItems: "center", justifyContent: "center", flexDirection: "row" }} >
            <TouchableOpacity
                onPress={() => navegacion.navigate('buscar')}
                style={styles.btnSearch}

            >
                <Text style={{ fontSize: 15, color: '#E9DDDA' }}>¿Qué estás buscando?</Text>
            </TouchableOpacity>
            <View style={{ marginLeft: 10 }}>
                <ShoppingCart />
            </View>
        </View>
    )
}

export default HeaderBasico

const styles = StyleSheet.create({
    inputContainer: {
        width: "85%",
        height: 30,
        borderWidth: 1,
        borderColor: "#fff",
        borderRadius: 20,
        backgroundColor: "#fff",
        margin: 5
    },
    inputStyle: {
        textAlign: "center",
        fontSize: 17,
        paddingBottom: 20
    },
    btnSearch: {
        backgroundColor: 'white',
        //borderColor: 'white',
        width: '90%',
        height: '40%',
        padding: '1%',
        borderRadius: 25,
        marginTop: 10,
        //top: '18%',
        alignItems: 'center',
        justifyContent: 'center'
    }
})
