import React from 'react';
import { TouchableOpacity} from 'react-native'
import { View } from 'react-native';
import { StyleSheet } from 'react-native';
import { Text } from 'react-native'
import { Icon } from 'react-native-elements'
import {useNavigation } from '@react-navigation/native';

const SearchBarra = () => {
    const navegacion = useNavigation();
    return (
        <View style={{ justifyContent: "center", alignItems: "center" }} >
            <View style={{ position: 'absolute', zIndex: 1, top: '1%', right: '70%', height: '0%', position: 'static' }} ><Icon name='search' size={21} color='#E9DDDA' /></View>
            <TouchableOpacity
                onPress={()=>navegacion.navigate('buscar')}
                style={styles.btnSearch}
            >
                <Text style={{ fontSize: 16, color: '#E9DDDA' }}>¿Qué estás buscando?</Text>
            </TouchableOpacity>
        </View>
    )

}

export default SearchBarra

const styles = StyleSheet.create({

    btnSearch: {
        backgroundColor: 'white',
        width: '100%',
        height: '55%',
        padding: '1%',
        borderRadius: 25,
        marginTop: 10,
        alignItems: 'center',
        justifyContent: 'center'
    },

})
