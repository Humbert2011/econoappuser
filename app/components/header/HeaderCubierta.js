import React, { useState } from 'react'
import { ActivityIndicator, StyleSheet, Text, TouchableOpacity, View, ScrollView} from 'react-native'
import { Image, Input, Icon } from 'react-native-elements';
import { FlatList } from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/native';

import ShoppingCart from './ShoppingCart';
import ProductCard from '../../components/ProductCard';
import Api from '../../utils/Api';
import { url } from '../../utils/config';
import { getValueFor } from '../../utils/localStorage';

const HeaderCubierta = ({ setVisible }) => {
    const [busquete, setBusquete] = useState('')
    const [productos, setProductos] = useState(null)
    const navegacion = useNavigation();
    const [loading, setLoading] = useState(false)

    const buscar = async () => {
        let token = null;
        await getValueFor("token").then(res => token = res);
        let uri = `${url}product/listarbusqueda/${busquete}/activo`;
        let api = new Api(uri, "GET", null, token);
        await api.call()
            .then(async (res) => {
                if (res.response) {
                    setProductos(res.result.data)
                    console.log(res.result.data)
                } else {
                    // setBusquete('Esto no funciona')
                    console.log('Esto no funciona')
                }
            });
    }

    let timer;
    const runTimer = () => {
        timer = setTimeout(async () => {
            setLoading(true);
            await buscar();
            setLoading(false);
        }, 1000);
    };

    const captura = (text) => {
        setBusquete(text)
        clearTimeout(timer);
        runTimer();
    };

    return (
        <View style={styles.firstContainer}>
            <View style={styles.secondContainer}>

                <View style={styles.thirdContainer}>
                    {/*ICONO*/}
                    <View style={styles.fourthContainer} >
                        <Image
                            resizeMode="stretch"
                            source={
                                require("../../../assets/logoBlanco.png")
                            }
                            style={styles.imglogo}
                        />
                    </View>
                    {/*CARRITO*/}
                    <View style={styles.fifthContainer} >
                        <ShoppingCart />
                    </View>
                </View>

                <View style={{ width: '100%', height: '40%', backgroundColor: '#3D5CA4' }}>
                    {/*BUSCADOR*/}
                    <View style={{ marginLeft: '5%', height: '70%', top: '20%' }}>
                        <Input
                            placeholder='¿Qué estás buscando?'
                            placeholderTextColor={'#E9DDDA'}
                            selectionColor={'#3D5CA4'}
                            onChange={(e) => captura(e.nativeEvent.text)}
                            // onKeyPress={() => buscar()}
                            allowFontScaling={false}
                            leftIcon={
                                <Icon
                                    name='search'
                                    size={18}
                                    color='#E9DDDA'
                                    padding={5}
                                />
                            }
                            inputContainerStyle={styles.sixthContainer}
                            inputStyle={{ fontSize: 16, textAlign: 'center' }}
                        />
                    </View>
                    {/*CANCELAR*/}
                    <View style={styles.seventhContainer}>
                        <TouchableOpacity
                            onPress={() => navegacion.goBack()}
                        >
                            <Text style={{ color: "#E9DDDA", fontSize: 16 }} allowFontScaling={false}>
                                Cancelar
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
            {/* <View style={{ width: '100%', height: '5%', backgroundColor: 'white' }}></View> */}
            {/*//<View style={{ alignItems: 'center', width: '105%', height: '83%', backgroundColor: 'white', marginTop: 10 }}>*/}
            <ScrollView>
                {
                    loading ?
                        <View style={{ height: '100%', width: '100%', alignItems: 'center', justifyContent: 'center' }} >
                            <ActivityIndicator size='large' color='#3D5CA4' />
                        </View>
                        : (
                            busquete.length === 0 ? (
                                <View style={{ width: '100%' }}>
                                    <ImagenNencontrada />
                                </View>
                            ) : (
                                <FlatList
                                    horizontal={false}
                                    numColumns={2}
                                    data={productos}
                                    renderItem={(producto) => (
                                        <Producto producto={producto} setVisible={setVisible} />
                                    )}
                                    keyExtractor={(item, index) => index.toString()}
                                />
                            )
                        )
                }
            </ScrollView>
        </View>
    )
}

const Producto = ({ producto, setVisible }) => {
    const { item } = producto;
    return (
        <View style={{ width: '48%', alignItems: "center", justifyContent: "center" }} key={producto.idproducto}>
            <ProductCard producto={item} setVisible={setVisible} />
        </View>
    )
}


function ImagenNencontrada() {
    return (
        <View style={styles.falloLoadImg}>
        </View>
    )
}

export default HeaderCubierta

const styles = StyleSheet.create({
    imglogo: {
        height: 36,
        width: 130
    },
    firstContainer: {
        width: '100%',
        height: '100%',
        backgroundColor: '#fff'
    },
    secondContainer: {
        backgroundColor: 'navy',
        width: '100%',
        height: 122
    },
    thirdContainer: {
        width: '100%',
        height: '60%',
        backgroundColor: '#3D5CA4',
        padding: '1%'
    },
    fourthContainer: {
        left: '6%',
        top: '48%',
        alignContent: 'flex-start',
        height: '95%',
        marginLeft: 5
    },
    fifthContainer: {
        flexDirection: 'row',
        height: '15%',
        top: '-2%',
        alignItems: 'flex-end',
        marginRight: 5,
        paddingLeft: '84%'
    },
    sixthContainer: {
        borderColor: '#3D5CA4',
        backgroundColor: 'white',
        borderRadius: 18,
        height: '63%',
        width: '75%'
    },
    seventhContainer: {
        width: '22%', 
        bottom: '5.5%',
        zIndex: 2,
        flexDirection: 'row',
        marginLeft: '80%',
        marginRight: 5
    },
    falloLoadImg: {
        top: 40,
        alignItems: 'center',
    }
})