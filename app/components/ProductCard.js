import React,{useState,useContext} from 'react'
import { StyleSheet, Text, View,ActivityIndicator,Alert,TouchableOpacity} from 'react-native'
import {Icon,Image, Button} from 'react-native-elements';
import {useNavigation} from '@react-navigation/native';

import { url } from '../utils/config';
import { getValueFor } from '../utils/localStorage';
import {addElementCart} from '../utils/cartStorage';
import Api from '../utils/Api';
import Loader from '../components/Loader';

import ToastAddCart from '../components/cart/ToastAddCart'

const ProductCard = ({producto}) => {
    const navigation = useNavigation();
    const {precio,idproducto,nombre,descripcion,informacion_adicional,urlImg} = producto;
    // truncar cadena de nombre 
    let newNombre = nombre.substring(0,37)
    const [showToast, setShowToast] = useState(false);
    const [loaderVisible, setloaderVisible] = useState(false)
    const [loaderButton, setLoaderButton] = useState(false)
    
    const addFavorito = async () => {
        setloaderVisible(true);
        let token= null;
        let idpersona = null;
        await getValueFor("token").then(res=>token = res);
        if(!token) {
            Alert.alert("Error","no has iniciado session",
            [
                {
                    text: "Acepto",
                    onPress: () => setloaderVisible(false),
                    style: "cancel"
                },
            ])
        }else{
            await getValueFor("id").then(res=> idpersona = res)
            let parametros = {
                idpersona,
                idproducto    
            }
            let uri = `${url}favoritos/addfavorite`;
            let api = new Api(uri,"POST",parametros,token);
            api.call().then(res => {
                if (res.response) {
                    Alert.alert("Producto agregado con exito","El producto se agrego de manera exitosa a tus favoritos",
                    [
                        {
                        text: "Acepto",
                        onPress: () => setloaderVisible(false),
                        style: "cancel"
                        },
                    ]
                    )
                }else{
                    Alert.alert("Error","No se pudo agregar el producto a tus favoritos",
                    [
                        {
                        text: "Acepto",
                        onPress: () => setloaderVisible(false),
                        style: "cancel"
                        },
                    ]
                    )
                }
            })
        }
    }

    const addFavoritos = () => {
        Alert.alert(
            "¿Quieres agregar este elemento a fovoritos?",
            "Estas seguro de que este producto es uno de tus favoritos",
            [
              {
                text: "Canselar",
                onPress: () => console.log("Cancel Pressed"),
                style: "cancel"
              },
              { text: "Si", onPress: () => addFavorito()}
            ]
          );
    }

    const addProductToCart = async () => {
        setLoaderButton(true);
        let add = await addElementCart(producto,1)
        await setTimeout(() => {
            setLoaderButton(false);
        },1000)
        add && setShowToast(true);
    }

    return (
        <TouchableOpacity 
            style ={styles.container} 
            onPress = {()=>navigation.navigate('detalleproducto',producto)}
        >
            {/* <View style={{width:'100%',backgroundColor:'#fff',alignItems:'flex-end'}} >
                <Icon onPress = {()=>addFavoritos(idproducto)} type="font-awesome" name = 'heart-o'  size = {20} />
            </View> */}
            <View style={{justifyContent:"center",alignItems:"center"}} >
                <Image
                    resizeMode = "stretch"
                    source={ urlImg ? { uri: urlImg } : require('../../assets/no-image.png') }
                    PlaceholderContent={<ActivityIndicator size="large" color="#3D5CA4"/>}
                    placeholderStyle = {{backgroundColor: '#fff'}}
                    style={{
                        height:95,
                        width : 95
                    }}
                />
            </View>
            <Text style={{fontWeight:'bold'}}>${precio}</Text>
            <View style={{height:45}}>
                <Text style={{fontSize:11}} >{newNombre}</Text>
            </View>
            <Button  
                containerStyle ={{marginTop: -4,height:35}}
                loading = {loaderButton}
                buttonStyle ={{height:35,borderWidth:0}} titleStyle={{fontSize:13,color:"#3D5CA4"}} 
                type="outline" 
                title="Agregar  "
                iconPosition="right"
                icon = {
                    <Icon
                        name='shopping-cart'
                        type='font-awesome'
                        color='#3D5CA4'
                        size = {18}
                    />
                }
                onPress = { () => addProductToCart()}
            />
            <Loader isVisible = {loaderVisible} />
            <ToastAddCart showToast = {showToast} setShowToast = {setShowToast} />
        </TouchableOpacity>
    )
}

export default ProductCard

const styles = StyleSheet.create({
    container: {
        height: 235,
        width: 160,
        backgroundColor:'#fff',
        borderColor: "#E7E7E7",
        borderWidth: 1,
        margin:4,
        padding : 10,
    }
})
