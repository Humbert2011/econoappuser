import React,{useState,useMemo} from 'react'
import { StyleSheet, Text, View } from 'react-native'
import {Icon,Overlay} from 'react-native-elements'

const ToastAddCart = ({showToast,setShowToast}) => {
    setTimeout(() => {
        setShowToast(false)
    }, 3000);
    return (   
        <Overlay
            isVisible={showToast}
            backdropStyle = {{backgroundColor: 'transparent'}}
            overlayStyle = {styles.overlay}
            onBackdropPress = {()=>setShowToast(false)}
        >
            <View style={styles.toast}>
                <Icon
                    name='check-circle-o'
                    type='font-awesome'
                    color='#fff'
                />
                <Text style={styles.text}>Se agregó al carro</Text>
            </View>
        </Overlay>
    )
}

export default ToastAddCart

const styles = StyleSheet.create({
    toastContainer: {
        bottom: 10,
        marginLeft : "3%",
        alignItems:"center",
        zIndex: 2000,
    },
    toast : {
        width : "90%",
        height : 40,
        backgroundColor : "#5CDC0A",
        borderRadius: 10,
        alignItems:"center",
        justifyContent: "center",
        flexDirection: "row"
    },
    text : {
        fontSize: 18,
        color: "#fff",
        marginLeft : 10
    },
    overlay:{
        flex: 1,
        position: 'absolute',
        left: 0,
        bottom: 50,
        width:"100%",
        backgroundColor:"transparent",
        alignItems:"center",
        borderRadius : 0,
        borderWidth : 0,
        shadowColor : 'transparent'
    },
})
