import React,{useEffect,useState} from 'react'
import { StyleSheet, Text, View,FlatList} from 'react-native'
import {Image,Icon} from 'react-native-elements'
import {size} from 'lodash';
import {deleteElementCart} from '../../utils/cartStorage'

const ListElementsCart = ({listElementsCart}) => {
    // console.log(listElementsCart);
    const [updateList, setUpdateList] = useState(false)
    useEffect(() => {
        if (updateList) {
            setUpdateList(false)
            listElementsCart = null;

        }
    }, [updateList])
    return (
        <View>
            {
                size(listElementsCart) > 0 ? (
                    <FlatList
                        data = {listElementsCart}
                        renderItem = { ( element ) => <Elemento element = {element} />  }
                        keyExtractor = { (item, index) => index.toString()}
                    />
                ) : (
                    <View style={{alignItems:"center"}} >
                        <Text>No hay elementos en el carrito.</Text>
                    </View>
                )
            }
        </View>
    )
}

const Elemento = ({element}) => {
    const {item } = element
    console.log(item);
    return (
    <View style={styles.contenedor} >
        <View style={styles.fila} >
           <View style={{width : "25%",alignItems:"center"}} >
                <Image
                    resizeMode = "stretch"
                    source = {
                        item.urlImg ? { uri:item.urlImg } : require("../../../assets/no-image.png")
                    }
                    style = {{width: 50, height: 50, marginRight:2 }}
                />
           </View>
           <View style={{width : "50%",marginRight:10}} >
             <Text style={{marginBottom:5}} >{item.nombre}</Text>
             <View style={styles.viewSumar}>
                    <Icon
                        containerStyle = {{paddingRight:'25%'}}
                        size = {17}
                        name='minus'
                        type='font-awesome'
                        color='#3D5CA4'
                        // onPress={() => (number!=1) && setNumber(number - 1)} 
                    />                   
                    <Text h4>{item.cantidad}</Text>
                    <Icon
                        containerStyle = {{paddingLeft:'25%'}}
                        size = {17}
                        name='plus'
                        type='font-awesome'
                        color='#3D5CA4'
                        // onPress={() => setNumber(number + 1)} 
                    /> 
                </View>
           </View>
           <View style={{width : "25%"}} >
                    <Text style={styles.textCosto}>${item.total}</Text>
                    <Text style={styles.textCosto}>${item.precio} c/u</Text>
                    <Text 
                        style={styles.elminar}
                        onPress={()=>deleteElementCart(item.idproducto)}
                    >Eliminar</Text>
           </View>
        </View>
        <View style={styles.fila} >
            
        </View>
    </View>)
}

export default ListElementsCart

const styles = StyleSheet.create({
    viewSumar : {
        width:"60%",
        height:45,
        borderRadius:7,
        borderWidth:1,
        borderColor:"grey",
        flexDirection:"row",
        justifyContent:"center",
        alignItems:"center",
        padding:2,
        marginRight : "5%"
    },
    contenedor: {
        width : '100%',
        height : 120,
        borderBottomColor : 'grey',
        borderBottomWidth : 1,
        marginTop : 10,
        margin : 10
    },
    fila: {
        flexDirection: "row"
    },
    textCosto :{
        fontWeight:'bold',
        textAlign:'right',
        marginRight:35,
        fontSize:16
    },
    elminar : {
        textAlign:'right',
        marginRight:35,
        color:'#3D5CA4',
        marginTop:10
    }
})
