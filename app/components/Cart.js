import React,{useState,useCallback} from 'react'
import { StyleSheet, Text, View, ScrollView} from 'react-native'
import {Overlay,Button,Icon,Header,Avatar,Input,Divider} from 'react-native-elements';
import { useFocusEffect,useNavigation } from '@react-navigation/native';
import {size} from 'lodash';

import ListElementsCart from './cart/ListElementsCart';
import {getValueFor} from '../utils/localStorage';


const Cart = (props) => {
    const {isVisible,setIsVisible,listElementsCart} = props;
    console.log(listElementsCart,'al entrar al carrito..');
    const [token, setToken] = useState(null)
    const navegacion = useNavigation();
    
    useFocusEffect(
        useCallback(() => {
            (async()=>{
                await getValueFor("token").then(res=>setToken(res));
            })()
        },[])
    )

    return(
        <Overlay
            isVisible={isVisible}
            fullScreen = {true}
            overlayStyle = {{
                padding : 0,
            }}
        >
            <View style={styles.header}>
                <Header
                    backgroundColor = "rgba(61,92,164,1)"
                    containerStyle = {{height:100}}
                    leftComponent={
                        <Icon
                        name="times"
                        type='font-awesome'
                        color='#fff'
                        onPress = { () =>setIsVisible(!isVisible)}
                        size = {27}
                    />
                    }
                    centerComponent={{ text: 'Carrito', style: { color: '#fff',fontSize:25 } }}
                />
            </View>
            <ScrollView style={styles.vista}>
                <View>
                    <Text style={{marginLeft:20,marginTop:20,fontSize:20,fontWeight:'bold'}}>Tu pedido</Text>
                    <View style= {{marginTop:20}}>
                       {/* <ListElementsCart listElementsCart = {listElementsCart} /> */}
                       {
                           size(listElementsCart)>0 ?
                           (listElementsCart.map((l, i) => (
                               console.log(l)
                           )))
                           :(
                               console.log('no hay elementos')
                           )
                       }
                    </View>
                </View>
                <View>
                    <Input placeholder="Agregar una nota." 
                            inputContainerStyle = {{borderBottomWidth:0,height:100,textAlign: "center"}}
                            inputStyle = {{textAlign: "center"}}
                    />
                    <Divider orientation="horizontal" />
                </View>
                <View style={{margin:20}} >
                    <View style = {{flexDirection:"row"}} >
                        <View style = {{width:"50%"}}><Text style={{fontSize:20}} >Subtotal</Text></View>
                        <View style = {{width:"50%",alignItems:"flex-end"}}><Text style={{fontSize:20}}>$35</Text></View>
                    </View>
                    <View style = {{flexDirection:"row",marginTop:20}} >
                        <View style = {{width:"50%"}}><Text style={{fontSize:20}} >Costo de envio</Text></View>
                        <View style = {{width:"50%",alignItems:"flex-end"}}><Text style={{fontSize:20}}>Gratis</Text></View>
                    </View>
                    <View style = {{flexDirection:"row",marginTop:20}}>
                    <View style = {{width:"50%"}} ><Text style={{fontSize:28}}>Total</Text></View>
                        <View style = {{width:"50%",alignItems:"flex-end"}}><Text style={{fontSize:28}}>$35</Text></View>
                    </View>
                    <Text style = {{fontStyle:"italic"}}>Los precios ya incluyen IVA</Text>
                </View>
                {/* <Text>Meto pago</Text> */}
        </ScrollView>
        {
            token ? (
                <Button
                    title="Continuar"
                    buttonStyle = {{backgroundColor:"rgba(61,92,164,1)",height:65}}
                    containerStyle = {styles.btnContinue}
                    onPress={()=>{
                        console.log("continuar")
                    }}
                />
            ) : (
                <Button
                    title="Inicia  Sesión"
                    buttonStyle = {{backgroundColor:"rgba(61,92,164,1)",height:65}}
                    containerStyle = {styles.btnContinue}
                    onPress={()=>{
                       navegacion.navigate("cuenta");
                       setIsVisible(false)
                    }}
            />
            )
        }
            
        </Overlay>
    );
}

export default Cart

const styles = StyleSheet.create({
    header: {
        // backgroundColor: "rgba(61,92,164,1)",
        // height:100,
        // flexDirection : "row",
        // paddingTop : 45,
    },
    vista:{
        paddingBottom:65,
        // flex:1,
        // alignItems: "center",
        // justifyContent: "center"
    },
    text:{
        color:"blue",
        textTransform:"uppercase",
        marginTop: 10
    },
    btnContinue : {
        // position : "absolute",
        // bottom: .1,
        width : "100%"
        // right : 1,
        // shadowColor : "black",
        // shadowOffset : { width: 2, height: 2 },
        // shadowOpacity : 0.5
    }
});
