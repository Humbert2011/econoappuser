import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";

function BarsTimeBlack(props) {
  return (
    <View style={[styles.container, props.style]}>
      <Text style={styles.time}>9:41</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  time: {
    height: 17,
    backgroundColor: "transparent",
    textAlign: "center",
    color: "rgba(0,0,0,1)",
    fontSize: 14,
    letterSpacing: -0.2800000011920929,
    marginTop: 7
  }
});

export default BarsTimeBlack;
