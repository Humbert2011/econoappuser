import React from 'react'
import { StyleSheet, Text, View, FlatList,TouchableOpacity,Dimensions,ActivityIndicator} from 'react-native'
import {useNavigation} from '@react-navigation/native';
import {Image} from 'react-native-elements';

import {size} from 'lodash';
import Loader from '../Loader';
const screen = Dimensions.get("screen").width;
const ListarCategorias = (props) => {
    const {categorias} = props;
    const navigation = useNavigation();
    return (
        <View>
        {
            size(categorias) > 0 ? (
              <FlatList
                horizontal = {false}
                numColumns = {2}
                data = {categorias}
                renderItem = { ( categoria ) => <Categoria categoria = {categoria}  navigation = {navigation} />  }
                keyExtractor = { (item, index) => index.toString()}
              />
            ):(
              <Loader
                isVisible ={true}
                text = "Cargando..."
              />
            )
        }
        </View>
    )
}


const Categoria = (props) => {
    const {categoria,navigation} = props;
    const {nombre,urlImg,idcategoria} = categoria.item;
    const  goTusubcatego = () => {
        navigation.navigate("listproductscategoria",{nombre,idcategoria});
    }
    return (
        <TouchableOpacity 
            onPress = {goTusubcatego}
        >
            <View style={styles.viewCategoria} > 
                <Image
                    resizeMode = "cover"
                    PlaceholderContent = { <ActivityIndicator color = "#FF6719" size = "small" />}
                    source = {
                        urlImg ? { uri:urlImg } : require("../../../assets/no-image.png")
                    }
                    style = { styles.imagenCategoria}
                />
                <Text>{nombre}</Text>
            </View>
        </TouchableOpacity>
    )
}

export default ListarCategorias

const styles = StyleSheet.create({
    viewCategoria : {
        height:115,
        width: (screen/2),
        borderRightColor: "#b7b7b7",
        borderRightWidth:.7,
        borderBottomColor:"#b7b7b7",
        borderBottomWidth:.7,
        justifyContent : "center",
        alignItems: "center",
        backgroundColor : "#ffffff",
    },
    imagenCategoria:{
        width:80,
        height: 80
    }
})
