import React,{useState, useCallback} from 'react'
import { StyleSheet,View, ScrollView,ActivityIndicator} from 'react-native'
import { useFocusEffect } from '@react-navigation/native';
import {map} from 'lodash';

import Api from '../../utils/Api';
import ProductCard from '../../components/ProductCard';
import {url} from '../../utils/config';
import {getValueFor} from '../../utils/localStorage';

const ListarProductsLimit = ({idsubcategoria}) => {
    
    const [productos, setproductos] = useState(null)

    useFocusEffect(
        useCallback(() => {
            (async()=>{
                let token= null;
                let id = null;
                await getValueFor("token").then(res=>token = res);
                let uri = `${url}product/listarprodcatapp/${idsubcategoria}/5/0`;
                let api = new Api(uri,"GET",null,token);
                api.call().then(res => {
                    // console.log(res);
                    if (res.response) {
                      setproductos(res.result);
                    }else{
                        (res.result == 401) && navegacion.navigate("cuenta");
                    }
                });
            })()
        },[])
    )

    return (
        <View>
            {
                !productos ? (
                    <View>
                        <ActivityIndicator size="large" color="black"/>
                    </View>
                ) : (
                    <ScrollView horizontal = {true}>
                        {
                            map(productos, (producto) => (
                                <ProductCard key={producto.idproducto} producto={producto}/>
                            ))
                        }
                    </ScrollView>
                )
            }
            
        </View>
    )
}

export default ListarProductsLimit

const styles = StyleSheet.create({})
