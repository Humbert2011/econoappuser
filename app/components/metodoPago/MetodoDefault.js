import React from 'react'
import { StyleSheet, Text, View, ScrollView,Alert} from 'react-native';
import {ListItem,Icon,CheckBox,Image,Button,Avatar} from 'react-native-elements';

const MetodoDefault = (props) => {
    const {metodoDefault} = props
    return (
        <View>
            {
                <ListItem 
                    key={99}
                    buttomDivider
                >
                    <Avatar 
                        source={{uri:metodoDefault.urlImg}}
                        style={{ width: 40, height: 20 }}
                    />
                    <ListItem.Content>
                        <ListItem.Title>{metodoDefault.Descripcion}</ListItem.Title>
                    </ListItem.Content>
                </ListItem>
            }
        </View>
    )
}

export default MetodoDefault

const styles = StyleSheet.create({})
