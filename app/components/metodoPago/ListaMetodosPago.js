import React from 'react'
import { StyleSheet, Text, View, ScrollView,Alert} from 'react-native';
import {ListItem,Icon,CheckBox,Image,Button,Avatar} from 'react-native-elements';

const ListaMetodosPago = (props) => {
    const {cambiarMetodoPago,metodosPago} = props;
    // console.log(metodosPago);
    return (
        <ScrollView>
            {
                metodosPago.map((item, i)=>(
                    <ListItem
                        key={i}
                        bottomDivider
                        onPress ={() => cambiarMetodoPago(item.idmetodoPago)}
                    >
                        <Avatar 
                            source={{uri:item.urlImg}}
                            resizeMode="contain"
                            style={{ width: 40, height: 20 }}
                        />
                        <ListItem.Content>
                            <ListItem.Title>{item.Descripcion}</ListItem.Title>
                        </ListItem.Content>
                    </ListItem>
                ))
            }
        </ScrollView>
    )
}

export default ListaMetodosPago

const styles = StyleSheet.create({})
