import React from 'react'
import { StyleSheet, Text, View, ScrollView,Alert} from 'react-native';
import {ListItem,Icon,CheckBox,Image,Button} from 'react-native-elements';

const ListaDirecciones = (porps) => {
    const {direcciones,actualizarDireccionDefecto,elimarDireccion} = porps;
    const confirmElimanr = (idDireccion) => {
        Alert.alert(
            "Eliminar Dirección",
            "¿Está seguro de quitar esta dirección?",
            [
                {
                    text:"Cancelar",
                    style: "cancel"
                },
                { 
                    text: "Eliminar", 
                    onPress: () => {
                        elimarDireccion(idDireccion)
                    } 
                }
            ],
            {
                cancelable:false
            }
        );
    }
    return (
        <ScrollView>
            {
                 direcciones.map((l, i) => (
                    <ListItem.Swipeable
                        key={i} 
                        bottomDivider 
                        onPress  = { () => actualizarDireccionDefecto(l.id_persona,l.iddirecciones)}
                        rightContent={
                            <Button
                              title="Eliminar"
                              icon={{ name: 'delete', color: 'white' }}
                              buttonStyle={{ minHeight: '100%', backgroundColor: 'red' }}
                              onPress = {()=>confirmElimanr(l.iddirecciones)}
                            />
                          }
                    >
                        <Icon
                            type="material-community"
                            name="google-maps"
                            color="#b7b7b7"
                        />
                        <ListItem.Content>
                            <ListItem.Title>{l.text_direc}</ListItem.Title>
                            <ListItem.Subtitle>{l.descripcion}</ListItem.Subtitle>
                        </ListItem.Content>
                        <CheckBox
                            center
                            checkedIcon={<Image style = {{width : 30,height:30}} source={require('../../../assets/selector.png')} />}
                            uncheckedIcon={<Image style = {{width : 30,height:30}} source={require('../../../assets/selector-vacio.png')} />}
                            checked={l.defecto == 1 ? true : false}
                            onPress = {()=>actualizarDireccionDefecto(l.id_persona,l.iddirecciones)}
                        />
                    </ListItem.Swipeable>
                  ))
            }
        </ScrollView>

    )
}

export default ListaDirecciones

const styles = StyleSheet.create({})
