import React, {useState} from 'react';
import {Platform} from "react-native";
import * as SQLite from "expo-sqlite";

const openDatabase = () => {
    if (Platform.OS === "web") {
      return {
        transaction: () => {
          return {
            executeSql: () => {},
          };
        },
      };
    }
  
    const db = SQLite.openDatabase("db.db");
    return db;
}
  
const db = openDatabase();
db.transaction((tx) => {
    tx.executeSql(
      "create table if not exists items (idproducto integer primary key not null, nombre text, precio int,cantidad int,total int,urlImg text);"
    );
});

const getCart = () => {
    return db
}

const addElementCart = async (producto,cantidad) => {
  try {
    const {idproducto,nombre,precio,urlImg} = producto;
    let total =(precio * cantidad).toFixed(2);
    // buscar elemento
    const addElement = async (item) => {
      console.log(item,'adsdas')
      if(!item){
        await db.transaction((tx) => {
          tx.executeSql("insert into items (idproducto, nombre, precio, cantidad, total, urlImg) values (? , ? , ? , ?, ? , ?)", [idproducto,nombre,precio,cantidad,total,urlImg]); 
        })
      }else{
        cantidad = item.cantidad + cantidad;
        total = (cantidad * precio).toFixed(2);
        console.log(total,'total')
        console.log(cantidad,'cantidad')
        await db.transaction((tx) => {
            tx.executeSql(`update items set precio = ?, cantidad = ?, total = ? where idproducto = ?;`, [precio,cantidad,total,idproducto])
          }
        )
      }
    };

    await db.transaction((tx) => {
      tx.executeSql("select * from items where idproducto = ?", [idproducto], (_, { rows: { _array } }) =>
        addElement(_array[0])
      );
    })
    return true;
  } catch (error) {
    return error;
  }
    
}

const deleteElementCart = async (id) => {
  console.log(id);
  try {
    await db.transaction(
      (tx) => {
        tx.executeSql(`delete from items where idproducto = ?;`, [id]);
      }
    )
    // console.log(`se elimino ${id}`);
    return true;
  } catch (error) {
    return false;
  }
}

export {addElementCart, deleteElementCart,getCart};