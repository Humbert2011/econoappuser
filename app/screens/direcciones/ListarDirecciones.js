import React,{useState,useCallback} from 'react'
import { StyleSheet, Text, View,ScrollView,ActivityIndicator,Alert} from 'react-native'
import {ListItem, Avatar} from 'react-native-elements';
import {useNavigation,useFocusEffect} from '@react-navigation/native';

import ListaDirecciones from '../../components/direcciones/ListaDirecciones';
import { getValueFor } from '../../utils/localStorage';
import Api from '../../utils/Api';
import {url} from '../../utils/config';

const ListarDirecciones = () => {
    const navegacion = useNavigation();
    const [direcciones, setDirecciones] = useState(null);
    const [token, setToken] = useState(null);
    const [reload, setReload] = useState(false);
    const [numD,setNumD] = useState(0);
    const listCuenta = [
        {
          title: 'Detectar mi ubicación actual o elegir una ubicación.',
          onPress: () => {navegacion.navigate("adddireccion",{numD})}
        }
      ]
    
      useFocusEffect(
        useCallback(() => {
            (async()=>{
                
                let token= null;
                let id = null;
                await getValueFor("token").then(res=> {
                                                            token = res
                                                            setToken(res);
                                                        });
                await getValueFor("id").then(res=> id  =res);
                let uri = `${url}direccion/listardireccion/${id}`;
                let api = new Api(uri,"GET",null,token);
                api.call().then(res => {
                    if (res.response) {
                        setDirecciones(res.result.data)
                        let len = res.result.data.length;
                        // console.log(len,'len=====')
                        setNumD(len)
                    }else{
                        (res.result == 401) && navegacion.navigate("espera");
                    }
                });
                setReload(false)
            })()
        },[reload])
    )
    
    const actualizarDireccionDefecto = async (idUser,idDirecciones) => {
        let uri = `${url}direccion/direccionDefecto/${idUser}/${idDirecciones}`;
        let api = new Api(uri,"PUT",null,token);
        await api.call().then(res => {
            if (res.response) {
                setDirecciones(null);
                setReload(true);
            }else{
                alert("Error al actualizar dirección.");
            }
        });
    } 

    const elimarDireccion = async (idDireccion) => {
        let uri = `${url}direccion/eliminardireccion/${idDireccion}`;
        let api = new Api(uri,"DELETE",null,token);
        await api.call().then(res => {
            if (res.response) {
                setDirecciones(null);
                setReload(true);
            }else{
                alert("Error al eliminar dirección.")
            }
        });
    }

    return (
        <ScrollView style={{backgroundColor:"#fff"}} > 
            <View>
                <Text style={{fontSize: 25,margin:15 }} > Agrega o selecciona una dirección para entrega. </Text>
            </View>
            <View>
            {
                listCuenta.map((item, i) => (
                    <ListItem key={i}  onPress= { item.onPress } >
                    <Avatar source = {require("../../../assets/logodireccion2.png")}  />
                    {/* <Logo width={120} height={40} /> */}
                    <ListItem.Content>
                        <ListItem.Title style={{fontSize:21}} >{item.title}</ListItem.Title>
                    </ListItem.Content>
                    <ListItem.Chevron iconStyle={{fontSize:50,}} />
                    </ListItem>
                ))
            }
            </View>
            
            {direcciones ? <ListaDirecciones direcciones = {direcciones} actualizarDireccionDefecto = {actualizarDireccionDefecto} elimarDireccion = { elimarDireccion } /> : <ActivityIndicator style = {{marginTop:30}} size="large" color="#FF6719" /> }
           {/* <Loader isVisible= {isVisible} text = "Cargando ..." />  */}
        </ScrollView>
    )
}

export default ListarDirecciones

const styles = StyleSheet.create({})
