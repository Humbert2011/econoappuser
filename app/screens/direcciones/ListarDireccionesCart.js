import React, { useState, useCallback } from 'react'
import { StyleSheet, Alert, View, ScrollView, ActivityIndicator } from 'react-native'
import { Button, Image, ListItem, Icon, Text, Avatar, CheckBox } from 'react-native-elements';
import { useFocusEffect, useNavigation } from '@react-navigation/native';

import { getCart } from '../../utils/cartStorage'
import { getValueFor, saveValue, deleteValue } from '../../utils/localStorage';
import Loader from '../../components/Loader';
;

import Api from '../../utils/Api';
import { url } from '../../utils/config';

const ListarDireccionesCart = () => {
  const [elementsCart, setElementsCart] = useState(null);
  const [id, setId] = useState(0);
  const [totalCompra, setTotalCompra] = useState(0);
  const [token, setToken] = useState(null);
  const [reload, setReload] = useState(false);
  const navegacion = useNavigation();
  const [direcciones, setDirecciones] = useState(null);
  const [idDireccion, setIdDireccion] = useState(0);
  const [isVisible, setIsVisible] = useState(false);
  const [numD,setNumD] = useState(0);

  const listCuenta = [
    {
      title: 'Elegir ubicación',
      onPress: () => navegacion.navigate('adddireccion',{numD})
    }
  ]

  useFocusEffect(
    useCallback(() => {
      (async () => {
        setReload(false)
        let tokenIn = null;
        await getValueFor("token").then(res => {
          tokenIn = res;
          setToken(res)
        });
        let db = getCart();
        await db.transaction((tx) => {
          tx.executeSql("select * from items", [], (_, { rows: { _array } }) =>
            _array.length == 0 ? navegacion.goBack() : setElementsCart(_array)
          );
        })
        await db.transaction((tx) => {
          tx.executeSql("select sum(total) as total from items", [], (_, { rows: { _array } }) =>
            setTotalCompra(_array[0].total)
          );
        })
        let id;
        await getValueFor("id").then(res => {
          setId(res);
          id = res;
        });
        let uri = `${url}direccion/listardireccion/${id}`;
        let api = new Api(uri, "GET", null, tokenIn);
        api.call().then(res => {
          if (res.response) {
            setDirecciones(res.result.data);
            let len = res.result.data.length;
            // console.log(len,'len=====')
            setNumD(len)
            let data = res.result.data;
            let direccionDefecto = data.find(data => data.defecto == 1);
            direccionDefecto && setIdDireccion(direccionDefecto.iddirecciones);
          }
        });

      })()
    }, [reload])
  )

  const actualizarDireccionDefecto = async (idUser, idDirecciones) => {
    let uri = `${url}direccion/direccionDefecto/${idUser}/${idDirecciones}`;
    let api = new Api(uri, "PUT", null, token);
    await api.call().then(res => {
      if (res.response) {
        setDirecciones(null);
        setReload(true);
      } else {
        alert("Error al actualizar dirección.");
      }
    });
  }

  const aprobasSend = () => {
    Alert.alert(
      "Confirmación",
      `Al dar click al botón Ok procederás a la confirmación de compra de tu pedido, ¿Proceder con mi pedido?`,
      [
        {
          text: "Cancelar",
          onPress: () => console.log("Cancel"),
          style: "cancel"
        },
        {
          text: "Ok", onPress: () => sendCart()
        }
      ]
    );
  }

  const sendCart = async () => {
    setIsVisible(true);
    let datallcar = [];
    elementsCart.map((l, i) => (
      datallcar.push({
        costo: l.precio,
        cantidad: l.cantidad,
        idproducto: l.idproducto
      })
    ));
    if (idDireccion == 0) {
      setIsVisible(false);
      Alert.alert(
        "No hay dirección seleccionada.",
        `Por favor, seleccione una dirección para poder dar de alta su pedido.`,
        [
          { text: "Ok", onPress: () => setIsVisible(false) }
        ]
      )
      return false;
    }
    let nota = await getValueFor('nota');
    let parametros = {
      total: totalCompra.toFixed(2),
      idpersona: id,
      iddireccion: idDireccion,
      nota: !nota ? '' : nota,
      detalle: datallcar
    };
    // console.log(parametros);
    // setIsVisible(false);
    let uri = `${url}pedido/addcarrito`;
    // console.log(uri);
    let api = new Api(uri, "POST", parametros, token);
    await api.call().then(async (res) => {
      if (res.response) {
        // bandera para saber si es una navegacion desde el carrito a mi cuenta
        await saveValue('movePedidos', '1')
        // borrar elementos de carrito
        await deleteValue('nota');
        let db = getCart();
        await db.transaction(
          (tx) => {
            tx.executeSql(`delete from items;`);
          }
        )
        setIsVisible(false);
        setTimeout(() => {
          navegacion.navigate('cuentaStack');
        }, 1);
      } else {
        alert("Error al dar de alta el pedido.");
      }
    });
  }

  return (
    <View style={{ height: '100%', width: '100%', backgroundColor: '#fff' }}>
      <View style={{ backgroundColor: "#fff", alignItems: 'flex-start', padding: 10, borderTopLeftRadius: 30, borderTopRightRadius: 30, }} >
        <Text h4>Confirmar la ubicación de entrega</Text>
      </View>
      <ScrollView>
        {
          !direcciones ? (
            <ActivityIndicator style={{ marginTop: 30 }} size="large" color="#FF6719" />
          ) : (
            direcciones.map((l, i) => (
              <ListItem
                key={i}
                bottomDivider
              // onPress  = { () => actualizarDireccionDefecto(l.id_persona,l.iddirecciones)}
              >
                <Icon
                  type="material-community"
                  name="google-maps"
                  color="#b7b7b7"
                />
                <ListItem.Content>
                  <ListItem.Title>{l.text_direc}</ListItem.Title>
                  <ListItem.Subtitle>{l.descripcion}</ListItem.Subtitle>
                </ListItem.Content>
                <CheckBox
                  center
                  checkedIcon={<Image style={{ width: 30, height: 30 }} source={require('../../../assets/selector.png')} />}
                  uncheckedIcon={<Image style={{ width: 30, height: 30 }} source={require('../../../assets/selector-vacio.png')} />}
                  checked={l.defecto == 1 ? true : false}
                  onPress={() => actualizarDireccionDefecto(l.id_persona, l.iddirecciones)}
                />
              </ListItem>
            ))
          )
        }
      </ScrollView>
      {
        listCuenta.map((item, i) => (
          <ListItem key={i} onPress={item.onPress} bottomDivider >
            <Avatar source={require("../../../assets/logodireccion2.png")} />
            {/* <Logo width={120} height={40} /> */}
            <ListItem.Content>
              <ListItem.Title style={{ fontSize: 21 }} >{item.title}</ListItem.Title>
            </ListItem.Content>
            <ListItem.Chevron iconStyle={{ fontSize: 50, }} />
          </ListItem>
        ))
      }
      <Button
        title="Confirmar ubicación de entrega."
        buttonStyle={{ backgroundColor: "rgba(61,92,164,1)", height: 65 }}
        onPress={() => aprobasSend()}
      />
      <Loader isVisible={isVisible} />
    </View>
  );
};

export default ListarDireccionesCart;

const styles = StyleSheet.create({});
