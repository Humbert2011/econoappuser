import React, {useState, useEffect,useRef} from 'react'
import {StyleSheet, Text, View , ActivityIndicator, Alert} from 'react-native'
import {Button,Input,Icon,Tooltip} from 'react-native-elements';
import * as Permissions from 'expo-permissions';
import * as Location from 'expo-location';
import MapView,{Marker,PROVIDER_GOOGLE} from 'react-native-maps';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import {useNavigation} from '@react-navigation/native';

import {getValueFor} from '../../utils/localStorage';
import Api from '../../utils/Api';
import {url} from '../../utils/config';
import Loader from '../../components/Loader';
import getDistanciaMetros from '../../utils/getDistanciaMetros'

const AddDireccion = ({route}) => {
    const {params} = route;
    console.log(params.numD);
    const [tolIsVisible, tolSetIsVisible] = useState(false);
    const navegacion = useNavigation();
    const [location, setLocation] = useState(null);
    const [nameDir, setNameDir] = useState(null);
    const [idUser, setIdUser] = useState(null);
    const [token, setToken] = useState(null);
    const [descripcionDir, setDescripcionDir] = useState(null);
    const [errors, setErrors] = useState({});
    const [isVisible, setIsVisible] = useState(false);
    const [econoUbicacion, setEconoUbicacion] = useState(null);
    const [distanciaMaxima, setDistanciaMaxima] = useState(null);
    const tooltipRef = useRef(null);

    useEffect(() => {
        params.numD == 0 && tooltipRef.current.toggleTooltip();
      }, []);

    useEffect(() => {
        (async()=>{
            let { status } = await Location.requestForegroundPermissionsAsync();
            if (status !== 'granted') {
                alert("Tienes que aceptar los permisos de localización.")
            }else{
                const loc = await Location.getCurrentPositionAsync({});
                setLocation({
                    latitude: loc.coords.latitude,
                    longitude : loc.coords.longitude,
                    latitudeDelta : 0.001,
                    longitudeDelta : 0.001
                })
            }
            // 
            await getValueFor('econoUbicacion').then(res =>setEconoUbicacion(res))
            await getValueFor('distanciaMaxima').then(res=>setDistanciaMaxima(res))
            // obtener id de usuario
            await getValueFor("token").then(res=>setToken(res));
            await getValueFor("id").then(res=>setIdUser(res));
        })()
    }, [])

    const obtenerDireccion = async () => {
        const direccion = await Location.reverseGeocodeAsync(location);
        let nameDireccion = `${direccion[0].street} ${direccion[0].name} ${(!direccion[0].district)?'':(direccion[0].district)} ${(!direccion[0].city)?'':(direccion[0].city)}`
        nameDireccion = nameDireccion.replace('null','');
        console.log(nameDireccion);
        setNameDir(nameDireccion);
    }

    const gurdarDireccion = async () => {
        setErrors({});
        if (!location) {
            alert("No hay ubicación en el mapa.")
        } else if(!nameDir) {
            setErrors({direccion:"Presione el ícono de mapa ->"})
        }else if(!descripcionDir ) {
                setErrors({descripcion:"Coloque una descripción de la dirección."})
        } else{
                setIsVisible(true);
                let coordenadasEcono = econoUbicacion.split(',');
                let distancia = getDistanciaMetros(location.latitude,location.longitude,coordenadasEcono[0],coordenadasEcono[1])
                if(distancia > distanciaMaxima){
                    let disKM = distancia / 1000;
                    let disMax = distanciaMaxima /1000;
                    setIsVisible(false);
                   Alert.alert(
                        `Distancia máxima de entrega es de ${disMax}km.`,
                        `tu ubicación está a ${disKM}km.`,
                        [
                          { text: "Ok", onPress:() => {
                              console.log("ok...");
                            }
                          }
                        ]
                      );
                }else{
                    let parametros = {
                            descripcion: descripcionDir,
                            text_direc : nameDir,
                            id_persona : idUser,
                            latitud : location.latitude,
                            longitud : location.longitude
                    }    
                    // console.log(parametros)
                    let uri = `${url}direccion/registrardireccion`;
                    let api = new Api(uri,"POST",parametros,token);
                    await api.call().then(res=>{
                        // console.log(res)
                        if (res.response) {
                            setIsVisible(false);
                            navegacion.goBack();
                        }else{
                            setIsVisible(false); 
                            alert("Error al dar de alta la dirección.");
                        }
                    })
                }
            }
    }

    return (
        <KeyboardAwareScrollView style={{backgroundColor:"#fff"}} >
           <View>
              {
                 (location) ?   
                    (
                        <MapView
                            style={styles.MapView}
                            initialRegion = {location}
                            showsMyLocationButton = {true}
                            showsCompass
                            provider={PROVIDER_GOOGLE}
                            showsUserLocation = {true}
                            onRegionChange={(region) => setLocation(region)} //para poder mover el marcador
                        >
                            <Marker 
                                    coordinate = { { latitude: location.latitude , longitude : location.longitude }  }
                                    image = {require("../../../assets/PinS.png")}
                                    draggable
                            />
                        </MapView>)
                    :(
                        <View 
                            style ={{width:"100%",height:300,justifyContent:"center",alignItems:"center"}}
                        >
                            <ActivityIndicator
                                size="large"
                                color="#FF6719"
                            />
                        </View>
                    )
              }
           </View>
            <View style={{marginTop:20}} >
               
                    <Input
                        label="Dirección"
                        labelStyle={{fontSize:16,fontWeight:"normal",color:"rgba(123,127,135,1)" }}
                        rightIcon={
                            // colocar el tooltip
                            <Tooltip 
                                pointerColor={"#3D5CA4"}
                                backgroundColor = "#3D5CA4"
                                popover ={<Text style={{color: "white"}}>Obtener dirección</Text>}
                                ref={tooltipRef} 
                            >
                                <Icon
                                    type="material-community"
                                    name="google-maps"
                                    color="#b7b7b7"
                                    // iconStyle={stilos.iconRigth}
                                    onPress={()=>obtenerDireccion()}
                                />
                            </Tooltip>
                        }
                        inputContainerStyle = {{borderBottomWidth:.5,borderColor:"#b7b7b7"}}
                        defaultValue = {nameDir}
                        selectionColor={'#3D5CA4'}
                        onChange = {(e)=>setNameDir(e.nativeEvent.text)}
                        errorMessage = {errors.direccion}
                    />
                    <Input
                        placeholder = "Instrucciones, por ejemplo: casa verde, etc."
                        inputContainerStyle = {{borderBottomWidth:.5,borderColor:"#b7b7b7"}}
                        inputStyle={{ fontSize: 16}}
                        onChange={(e)=>setDescripcionDir(e.nativeEvent.text)}
                        errorMessage = {errors.descripcion}
                        selectionColor={'#3D5CA4'}
                    />
                    <Button
                        buttonStyle={{backgroundColor:"#3D5CA4"}}
                        containerStyle = {{height:50}}
                        title="Confirmar"
                        onPress = {()=>gurdarDireccion()}
                    />
            </View>
            <Loader  isVisible = {isVisible} text = "Cargando..."  />
        </KeyboardAwareScrollView>
    )
}

export default AddDireccion

// const styles = StyleSheet.create({
//     container: {
//       flex: 1,
//       backgroundColor: '#fff',
//       alignItems: 'center',
//       justifyContent: 'center',
//     },
//   });

const styles = StyleSheet.create({
    MapView:{
        width:"100%",
        height:300
    },
    placeholder:{
        
    },
    ColPopover:{
      backgroundColor: '#3D5CA4'
    }
})
