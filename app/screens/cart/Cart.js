import React, { useState, useCallback } from 'react'
import { StyleSheet, Alert, View, ScrollView, ActivityIndicator } from 'react-native'
import { Button, Image, Input, Divider, Icon, Text } from 'react-native-elements';
import { useFocusEffect, useNavigation } from '@react-navigation/native';
import { size } from 'lodash';

import { getCart, deleteElementCart } from '../../utils/cartStorage'
import { getValueFor, saveValue } from '../../utils/localStorage';
import LoaderLineal from '../../components/LoaderLineal';

const Cart = () => {
    const [elementsCart, setElementsCart] = useState(null);
    const [nota, setNota] = useState('');
    const [token, setToken] = useState(null);
    const [totalCompra, setTotalCompra] = useState(0);
    const [reload, setReload] = useState(false);
    const navegacion = useNavigation();

    useFocusEffect(
        useCallback(() => {
            (async () => {
                let tokenIn = null;
                await getValueFor("token").then(res => {
                    tokenIn = res;
                    setToken(res)
                });
                await getValueFor("nota").then(res => {
                    res && setNota(res)
                });
                let db = getCart();
                setReload(false)
                await db.transaction((tx) => {
                    tx.executeSql("select * from items", [], (_, { rows: { _array } }) =>
                        setElementsCart(_array)
                    );
                })
                await db.transaction((tx) => {
                    tx.executeSql("select sum(total) as total from items", [], (_, { rows: { _array } }) =>
                        setTotalCompra(_array[0].total)
                    );
                })

            })()
        }, [reload])
    )
    // funcion continuar
    const continuar = async () => {
        let compraMinimna = await getValueFor('campraMinima');
        if (totalCompra < compraMinimna) {
            Alert.alert(
                `Carrito de compra`,
                `La compra mínima es de $${compraMinimna}`,
                [
                    {
                        text: "OK", onPress: () => {
                            console.log("OK");
                        }
                    }
                ]
            )
        } else {
            saveValue('nota', nota)
            navegacion.navigate('listardireccionesCart')
        }
    }
    // capturar nota 
    const onchange = (e) => {
        saveValue('nota', nota)
        setNota(e.nativeEvent.text)
    };
    // elimnar elemento del carrito
    const deleteElement = (idproducto, nombre) => {
        Alert.alert(
            "¿Quieres eliminar este producto del carrito?",
            `${nombre}`,
            [
                {
                    text: "Cancelar",
                    onPress: () => console.log("Cancel"),
                    style: "cancel"
                },
                {
                    text: "Si", onPress: () => {
                        deleteElementCart(idproducto);
                        console.log("elminando...");
                        setReload(true)
                    }
                }
            ]
        );
    }

    return (
        <View style={{ width: '100%', height: '100%', backgroundColor: '#fff' }} >
            {
                (size(elementsCart) > 0) ?
                    <ScrollView style={styles.vista}>
                        <View>
                            <Text style={{ marginLeft: 20, marginTop: 20, fontSize: 20, fontWeight: 'bold' }}>Tu pedido</Text>
                            <View style={{ marginTop: 20 }}>
                                {
                                    !elementsCart ? (
                                        <LoaderLineal />
                                    ) : (
                                        size(elementsCart) > 0 ?
                                            (
                                                elementsCart.map((l, i) => (
                                                    <View style={styles.contenedor} key={i} >
                                                        <View style={styles.fila} >
                                                            <View style={{ width: "25%", alignItems: "center" }} >
                                                                <Image
                                                                    resizeMode="stretch"
                                                                    PlaceholderContent={<ActivityIndicator size='small' color="#3D5CA4" />}
                                                                    placeholderStyle={{ backgroundColor: '#fff' }}
                                                                    source={
                                                                        l.urlImg ? { uri: l.urlImg } : require("../../../assets/no-image.png")
                                                                    }
                                                                    style={{ width: 50, height: 50, marginRight: 2 }}
                                                                />
                                                            </View>
                                                            <View style={{ width: "50%", marginRight: 10 }} >
                                                                <Text style={{ marginBottom: 5 }} >{l.nombre}</Text>
                                                                <View style={styles.viewSumar}>
                                                                    <Text h4>{l.cantidad}</Text>
                                                                </View>
                                                            </View>
                                                            <View style={{ width: "25%" }} >
                                                                <Text style={styles.textCosto}>${l.total}</Text>
                                                                <Text style={styles.textCosto}>${l.precio} c/u</Text>
                                                                <Text
                                                                    style={styles.elminar}
                                                                    onPress={() => deleteElement(l.idproducto, l.nombre)}
                                                                >Eliminar</Text>
                                                            </View>
                                                        </View>
                                                        <View style={styles.fila} >

                                                        </View>
                                                    </View>
                                                ))
                                            )
                                            : (
                                                <View style={{ alignItems: "center", justifyContent: "center" }} >
                                                    <Icon
                                                        name='cart-outline'
                                                        type='ionicon'
                                                        color='#CFCFCF'
                                                        size={70}
                                                    />
                                                    <Text style={{ color: '#CFCFCF', fontSize: 20 }} >
                                                        Tú carrito está vacío.
                                                    </Text>
                                                </View>
                                            )
                                    )
                                }
                            </View>
                        </View>
                        <View>
                            <Input placeholder="Agregar una nota."
                                inputContainerStyle={{ borderBottomWidth: 0, height: 100, width:'100%', textAlign: "center" }}
                                inputStyle={{ textAlign: "center" }}
                                selectionColor={'#3D5CA4'}
                                onChange={(e) => onchange(e)}
                                defaultValue={nota}
                            />
                            <Divider orientation="horizontal" />
                        </View>
                        <View style={{ margin: 20 }} >
                            <View style={{ flexDirection: "row" }} >
                                <View style={{ width: "50%" }}><Text style={{ fontSize: 20 }} >Subtotal</Text></View>
                                <View style={{ width: "50%", alignItems: "flex-end" }}><Text style={{ fontSize: 20 }}>${!totalCompra ? 0 : totalCompra.toFixed(2)}</Text></View>
                            </View>
                            <View style={{ flexDirection: "row", marginTop: 20 }} >
                                <View style={{ width: "50%" }}><Text style={{ fontSize: 20 }} >Costo de envio</Text></View>
                                <View style={{ width: "50%", alignItems: "flex-end" }}><Text style={{ fontSize: 20 }}>Gratis</Text></View>
                            </View>
                            <View style={{ flexDirection: "row", marginTop: 20 }}>
                                <View style={{ width: "50%" }} ><Text style={{ fontSize: 28 }}>Total</Text></View>
                                <View style={{ width: "50%", alignItems: "flex-end" }}><Text style={{ fontSize: 28 }}>${!totalCompra ? 0 : totalCompra.toFixed(2)}</Text></View>
                            </View>
                            <Text style={{ fontStyle: "italic" }}>Los precios ya incluyen IVA</Text>
                            <View style={{ flexDirection: "row", marginTop: 20 }} >
                                <Image
                                    source={require("../../../assets/efectivo.png")}
                                    style={{ height: 20, width: 40 }}
                                    resizeMode="stretch"
                                />
                                <View style={{ width: "50%", marginLeft: 10 }}><Text style={{ fontSize: 20 }} > Pago en Efectivo</Text></View>
                            </View>
                        </View>
                    </ScrollView>
                    : (
                        <View style={{ width: '100%', height: '91.1%', backgroundColor: '#fff' }}>
                            <Text style={{ marginLeft: 20, marginTop: 20, fontSize: 20, fontWeight: 'bold' }}>Tu pedido</Text>
                            <View style={{ marginTop: '50%' }}>
                                <View style={{ alignItems: "center", justifyContent: "center" }} >
                                    <Icon
                                        name='cart-outline'
                                        type='ionicon'
                                        color='#CFCFCF'
                                        size={70}
                                    />
                                    <Text style={{ color: '#CFCFCF', fontSize: 20 }} >
                                        Tú carrito está vacío.
                                    </Text>
                                </View>


                            </View>
                        </View>
                    )
            }
            {
                token ? (
                    <View>
                        <Button
                            title={`Continuar $${!totalCompra ? 0 : totalCompra.toFixed(2)}`}
                            buttonStyle={{ backgroundColor: "rgba(61,92,164,1)", height: 65 }}
                            containerStyle={styles.btnContinue}
                            onPress={() => continuar()}
                        />
                    </View>
                ) : (
                    <Button
                        title="Inicia Sesión"
                        buttonStyle={{ backgroundColor: "rgba(61,92,164,1)", height: 65 }}
                        containerStyle={styles.btnContinue}
                        onPress={() => {
                            navegacion.navigate("cuentaStack");
                        }}
                    />
                )
            }
        </View>
    )
}

export default Cart

const styles = StyleSheet.create({
    viewSumar: {
        width: "50%",
        height: 45,
        borderRadius: 7,
        borderWidth: 1,
        borderColor: "#BEBEBE",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        padding: 2,
        marginRight: "5%"
    },
    contenedor: {
        width: '100%',
        height: 120,
        borderBottomColor: 'grey',
        borderBottomWidth: 1,
        marginTop: 10,
        margin: 10
    },
    fila: {
        flexDirection: "row"
    },
    textCosto: {
        fontWeight: 'bold',
        textAlign: 'right',
        marginRight: 35,
        fontSize: 16
    },
    elminar: {
        textAlign: 'right',
        marginRight: 35,
        color: '#3D5CA4',
        marginTop: 10
    }
})
