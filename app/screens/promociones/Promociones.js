import React,{useState,useCallback} from 'react'
import { StyleSheet, Text, View, FlatList } from 'react-native'
import { useFocusEffect,useNavigation } from '@react-navigation/native';
import {size} from 'lodash';

import ProductCard from '../../components/ProductCard';
import LoaderLineal from '../../components/LoaderLineal'
import Api from '../../utils/Api';
import {url} from '../../utils/config';
import {getValueFor} from '../../utils/localStorage';


const Promociones = () => {
    const navegacion = useNavigation();
    const [productos, setproductos] = useState(null);
    useFocusEffect(
        useCallback(() => {
            (async()=>{
                let token= null;
                await getValueFor("token").then(res=>{
                    token = res
                });
                // if (token){
                    let uri = `${url}product/promociones`;
                    let api = new Api(uri,"GET",null,token);
                    await api.call().then(res=> {
                        console.log(res)
                        if (res.response) {
                            setproductos(res.result.promociones)
                        }
                    })
                // }
            })()
        },[])
    )

    return (
        
        !productos ? (
            <View style = { {width: '100%', height: '100%', backgroundColor: '#fff',alignItems:"center",justifyContent:"center"}}>
                <LoaderLineal/>
            </View>
        ) : (
            <View style={{padding:10,paddingBottom:50,backgroundColor:'#fff',height:'100%'}}>
                <Text style={{fontSize:19,fontWeight:'bold',marginBottom:20}}>Promociones</Text>
                {
                    size(productos) > 0 ? (
                    <FlatList
                        horizontal = {false}
                        numColumns = {2}
                        data = {productos}
                        renderItem = { ( producto ) => <Producto producto = {producto} />  }
                        keyExtractor = { (item, index) => index.toString()}
                        style = {{backgroundColor:"#fff",height:'100%',marginBottom:-40,marginTop:-10}}
                    />
                    ):(
                        <View style ={{width:'100%',height:'100%',alignItems: 'center',justifyContent: 'center'}}>
                            <Text style ={{color:'grey'}}>No hay productos</Text>
                        </View>
                    )
                }
                {/* <Icon
                    type = "material-community"
                    name = "filter"
                    color = "#FF6719"
                    reverse
                    containerStyle = { styles.btnContinerFilter }
                    onPress = { () => { console.log('abrir filtros')}}
                /> */}
            </View>
        )
    )
}
const Producto = ({producto}) => {
    const {item} = producto;
    return(
        <View style ={{width:'50%',alignItems:"center",justifyContent:"center"}}>
            <ProductCard producto = {item}/>
        </View>
    )
}
export default Promociones

const styles = StyleSheet.create({})
