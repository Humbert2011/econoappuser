import React,{useState,useCallback} from 'react'
import { StyleSheet, Text, View, ScrollView,FlatList} from 'react-native'
import { useFocusEffect } from '@react-navigation/native';
import {Icon} from 'react-native-elements';
import {size} from 'lodash';

import ProductCard from '../../components/ProductCard';
import LoaderLineal from '../../components/LoaderLineal'
import Api from '../../utils/Api';
import {url} from '../../utils/config';
import {getValueFor} from '../../utils/localStorage';

const ListForSubcategorias = ({route}) => {
    const {idsubcategoria} = route.params;
    const [productos, setproductos] = useState(null)
    const [totalProductos, settotalProductos] = useState(0)
    
    useFocusEffect(
        useCallback(() => {
            (async()=>{
                let token= null;
                await getValueFor("token").then(res=>token = res);
                let uri = `${url}product/listarprodsubcatapp/${idsubcategoria}`;
                let api = new Api(uri,"GET",null,token);
                api.call().then(res => {
                    // console.log(res);
                    if (res.response) {
                        setproductos(res.result.productos);
                        settotalProductos(res.result.total)
                    }else{
                        (res.result == 401) && navegacion.navigate("cuenta");
                    }
                });
            })()
        },[])
    )


    return (
        
        !productos ? (
            <View style = { {width: '100%', height: '100%', backgroundColor: '#fff',alignItems:"center",justifyContent:"center"}}>
                <LoaderLineal/>
            </View>
        ) : (
            <View style={{padding:10,paddingBottom:50,backgroundColor:'#fff',height:'100%'}}>
                <Text style={{fontSize:19,fontWeight:'bold',marginBottom:20}}>{totalProductos} productos</Text>
                {
                    size(productos) > 0 ? (
                    <FlatList
                        horizontal = {false}
                        numColumns = {2}
                        data = {productos}
                        renderItem = { ( producto ) => <Producto producto = {producto} />  }
                        keyExtractor = { (item, index) => index.toString()}
                        style = {{backgroundColor:"#fff",height:'100%',marginBottom:-40,marginTop:-10}}
                    />
                    ):(
                        <Text>No hay productos</Text>
                    )
                }
                {/* <Icon
                    type = "material-community"
                    name = "filter"
                    color = "#FF6719"
                    reverse
                    containerStyle = { styles.btnContinerFilter }
                    onPress = { () => { console.log('abrir filtros')}}
                /> */}
            </View>
        )
    )
}

const Producto = ({producto}) => {
    const {item} = producto;
    return(
        <View style ={{width:'50%',alignItems:"center",justifyContent:"center"}}>
            <ProductCard producto = {item}/>
        </View>
    )
}

export default ListForSubcategorias

const styles = StyleSheet.create({
    btnContinerFilter : {
        position : "absolute",
        bottom: 10,
        right : 10,
        shadowColor : "black",
        shadowOffset : { width: 2, height: 2 },
        shadowOpacity : 0.5
    }
})
