import React, { useState,useCallback } from "react";
import { StyleSheet, View,ScrollView,Dimensions,Text } from "react-native";
import { useFocusEffect } from '@react-navigation/native';
import { Chip } from 'react-native-elements';
import {map} from 'lodash';

import Api from '../../utils/Api';
import {url} from '../../utils/config';
import {getValueFor} from '../../utils/localStorage';
import Loader from '../../components/Loader';
import ListarProductsLimit from '../../components/categorias/ListarProductsLimit';

const widthScreen = Dimensions.get("window").width;
const widthMidel = (widthScreen / 2);

function ListProductsCategoria(props) {
  const {navigation, route} = props;
  // const {nombre,idcategoria} = route.params;
  const nombre = 'Frutas y Verduras';
  const idcategoria = 3;
  const [subcategorias, setSubcategorias] = useState(null)
 
  useFocusEffect(
    useCallback(() => {
        (async()=>{
            let token= null;
            await getValueFor("token").then(res=>token = res);
            let uri = `${url}product/listarsubcategorias/${idcategoria}`;
            let api = new Api(uri,"GET",null,token);
            api.call().then(res => {
                if (res.response) {
                  setSubcategorias(res.result.data)
                }else{
                    (res.result == 401) && navegacion.navigate("cuenta");
                }
            });
        })()
    },[])
)

return (
    <ScrollView style={{backgroundColor:"#fff"}}>
      {
        subcategorias == null ? <Loader text="Cargando ..." /> : (
          <View>
            <ScrollView  horizontal = {true}>
                {
                  map(subcategorias, (subcategoria, index) => (
                    <Chip
                    key = {subcategoria.idsubcategoria}
                    title = {`${subcategoria.descripcion}`}
                    titleStyle = {{color : 'black'}}
                    containerStyle = {{margin:10,borderRadius : 20}}
                    buttonStyle = {{backgroundColor:'#E7E7E7'}}
                    onPress = {()=>navigation.navigate("prodforsucategorias",{nombre,'subcategoria':subcategoria.descripcion,'idsubcategoria':subcategoria.idsubcategoria})}
                  />
                ))
                }
            </ScrollView>
            <ScrollView style={{marginBottom:20}}>
            {
                  map(subcategorias, (subcategoria,index) => (
                    <View key={index.toString()} style={{marginTop:10}}>
                      <View style={{flexDirection: 'row', justifyContent: 'flex-end',marginLeft:10}}>
                        <View style={{width:widthMidel}} >
                          <Text style={{fontSize:19,fontWeight:'bold',flexDirection: "row",marginLeft:10}}>{subcategoria.descripcion}</Text>
                        </View>
                        <View style={{width: widthMidel,textAlign:"right",alignItems:'flex-end'}}>
                          <Text onPress ={()=>{navigation.navigate("prodforsucategorias",{nombre,'subcategoria':subcategoria.descripcion,'idsubcategoria':subcategoria.idsubcategoria})}} style={{color:'rgba(61,92,164,1)',marginRight:10}}>Ver todo</Text>
                        </View>
                      </View>
                      <View style={{marginLeft:10}}>
                        <ListarProductsLimit idsubcategoria={subcategoria.idsubcategoria} />
                      </View>
                    </View>
                ))
            }
            </ScrollView>
          </View>
        )
      }
    </ScrollView>
  );
}

export default ListProductsCategoria;
