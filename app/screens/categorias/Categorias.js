import React, {useEffect,useState,useCallback} from 'react'
import { StyleSheet, Text, View, ScrollView, Dimensions } from 'react-native'
import { useFocusEffect } from '@react-navigation/native';


import Api from '../../utils/Api';
import {url} from '../../utils/config';

import ListarCategorias from '../../components/categorias/ListarCategorias';

const widthScreen = Dimensions.get("window").width;
const widthCategoria = (widthScreen / 2) - 4;
const Categorias = () => {
  
  const [categorias, setCategorias] = useState([]);
  // console.log(categorias);
  useEffect(() => {
    let uri = `${url}product/listarcategoria`;
    let api = new Api(uri,"GET");
    api.call().then((res)=>{
      if (res.response) {
          setCategorias(res.result.data);
      }
    })
  }, [])

  return (
    <View style ={styles.ScrollViewCategorias} >
      <ListarCategorias
        categorias = { categorias }
      />
    </View>
  )
}

export default Categorias

const styles = StyleSheet.create({
  categoria:{
    width:widthCategoria,
    height:120,
    backgroundColor:"#fff",
    margin:2,
    flexDirection : "row"
  },
  ScrollViewCategorias:{
    
  },
  contenedorDobles :{
    flexDirection:'row',
    justifyContent:"center",
    alignItems:"center"
  }
})
