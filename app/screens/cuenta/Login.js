import React,{useState} from 'react'
import {useNavigation} from '@react-navigation/native';
import { StyleSheet, Text, View } from 'react-native'
import {Input,Button,Icon,Avatar,Image} from 'react-native-elements';

import Api from '../../utils/Api';
import {url} from '../../utils/config';
import Loader from '../../components/Loader';
import {saveValue,getValueFor} from '../../utils/localStorage';

const Login = () => {
    const navegacion = useNavigation();
    const [parametros, setParametros] = useState({
        user: '',
        password: '',
        token: '',
        plataforma:"Movil"
    });
    const [verPassword, setVerPassword] = useState(false);
    const [isVisible, setIsVisible] = useState(false)
    const [errors, setErrors] = useState({});

    const onchange = (e,tipo) =>{
        setParametros({...parametros,[tipo]:e.nativeEvent.text})
    }

    const iniciarSesion = async () =>{
        setErrors({});
        let tokenPush = null;
        await getValueFor('tokenPush').then(res=>tokenPush = res);
        let data = parametros;
        data.token = `${tokenPush}`;
        let uri = `${url}auth/loginUser`;
        // console.log(data);
        let api = new Api(uri,"POST",data);
        setIsVisible(true);
        await api.call()
                 .then( async (res) => {
                     console.log(res)
                    if (res.response) {
                        await saveValue("token",res.result.token);
                        await saveValue("id",res.result.id);
                        setIsVisible(false);
                        navegacion.navigate("espera");
                    } else {
                        setIsVisible(false)
                        setErrors({password:`${res.errors}`})
                    }
                 });
    }

    return (
        <View style = {{backgroundColor : "#fff", height:"100%"}} >
            <View style={{margin:20}} >
                <Text style={{fontSize: 24}} >Bienvenido 👋</Text>
                <Text style={styles.estamosContentosDe}>
                    Estamos contentos de verte de nuevo. {"\n"}Inicia sesión con tu cuenta.
                </Text>
            </View>
            <View style ={{justifyContent:"center",alignItems:"center"}} >
                <Input
                    placeholder = "Número o correo"
                    style = {styles.inputStyle}
                    selectionColor={'#3D5CA4'}
                    containerStyle = {styles.inputContaine}
                    inputContainerStyle={{borderBottomWidth:0}}
                    onChange={(e)=>onchange(e,"user")}
                />
                <Input
                    placeholder = "Contraseña"
                    style = {styles.inputStyle}
                    containerStyle = {styles.inputContaine}
                    selectionColor={'#3D5CA4'}
                    inputContainerStyle={{borderBottomWidth:0}}
                    onChange={(e)=>onchange(e,"password")}
                    password={true}
                    secureTextEntry={verPassword ? false : true}
                    rightIcon={
                        <Icon
                            type="font-awesome" 
                            name = {verPassword ? "eye-slash" : "eye"}
                            size={18}
                            iconStyle = {{color:"gray"}}
                            onPress={()=>setVerPassword(!verPassword)}
                        />
                    }
                    errorMessage = {errors.password}
                />
                </View>
                {/* <View>
                    <Text 
                        style={{marginBottom:25,marginTop:4,marginRight:20,color: "rgba(151,158,181,1)",textAlign:"right"}} 
                        onPress={()=>navegacion.navigate("recuperarPassword")}
                    >¿Olvidaste tu contraseña?</Text>
                </View> */}
                <View style ={{justifyContent:"center",alignItems:"center"}} >
                <Button
                    containerStyle={styles.containerIngresar}
                    buttonStyle = {styles.btnIngresar}
                    title="Iniciar"
                    titleStyle ={{fontSize:23,letterSpacing: -0.5750000000000001,}}
                    onPress = {()=>iniciarSesion()}
                />
                <Text 
                    style={{color: "rgba(151,158,181,1)",marginTop:10}}  
                    onPress={()=>navegacion.navigate("ingresaNumero")}
                >¿No tienes cuenta? Crea una.</Text>
            </View>
            <Loader  isVisible ={ isVisible } text = "Cargando..."/>
        </View>
    )
}

export default Login

const styles = StyleSheet.create({
    estamosContentosDe:{
        color: "rgba(151,158,181,1)",
        fontSize: 16,
        letterSpacing: -0.4
    },
    containerIngresar:{
        width:"90%",
        height:50,
        fontSize : 23
    },
    btnIngresar : {
        borderRadius: 6,
        backgroundColor: "#3D5CA4",
        fontSize : 23
    },
    inputContaine : {
        width : "90%",
        height:49,
        borderWidth: 1,
        borderColor: "rgba(151,151,151,1)",
        borderRadius: 6,
        backgroundColor: "rgba(255,255,255,1)",
        marginBottom:20
    },
    inputStyle:{
        textAlign:"center",
        fontSize:18,
    }
})

