import React, {useState} from 'react'
import { StyleSheet, Text, View } from 'react-native'
import {Input,Button,Icon,Avatar,Image} from 'react-native-elements';
import {useNavigation} from '@react-navigation/native';
import {size} from 'lodash';

import {deleteValue} from '../../../utils/localStorage';
import Loader from '../../../components/Loader';
import Api from '../../../utils/Api';
import {url} from '../../../utils/config';

const ValidarCodigo = (props) => {
    const navegacion = useNavigation();

    const {route} = props;
    const {telefono,codigo_pais,tipoPersona} = route.params;
    
    const [isVisible, setIsVisible] = useState(false)
    const [errors, setErrors] = useState({});
    const [parametros, setParametros] = useState({
            codigo_pais:codigo_pais,
            telefono: telefono,
            codigo: null,
            tipoPersona:tipoPersona
    })

    const onchange = (e,tipo) =>{
        setParametros({...parametros,[tipo]:e.nativeEvent.text})
    };

    const validarCodigo = async () => {
        setErrors({});
        console.log(parametros)
        if (!parametros.codigo) {
            setErrors({codigo:"El código no puede estar vacío."});
        } else if(size(parametros.codigo)<4){
            setErrors({codigo:"Hacen falta dígitos en el código."});
        }else{
            setIsVisible(true);
            let uri = `${url}auth/validateCode`;
            let api = new Api(uri,"POST",parametros);
            await api.call().then(res=>{
                if (!res.response) {
                    setIsVisible(false);
                    deleteValue("telTemp");
                    setErrors({codigo:res.errors});
                }else{
                    navegacion.navigate("registro",{telefono,codigo_pais,tipoPersona,tokenTemReg:res.result})
                    setIsVisible(false);
                }
            });
        }
    };

    return (
        <View style = {{backgroundColor : "#fff", height:"100%"}} >
            <View style={{margin:20}} >
                <Text style={{fontSize: 24}} >Confirmación de código.</Text>
                <Text style={styles.porFavor}>
                    Por favor ingresa el código que fue enviado al teléfono. {telefono}
                </Text>
            </View>
            <View style ={{justifyContent:"center",alignItems:"center",marginTop:20}} >
                <Input
                        placeholder = "Código"
                        style = {styles.inputStyle}
                        containerStyle = {styles.inputContaine}
                        selectionColor={'#3D5CA4'}
                        inputContainerStyle={{borderBottomWidth:0}}
                        keyboardType= "numeric"
                        onChange={(e)=>onchange(e,"codigo")}
                        errorMessage = {errors.codigo}
                        maxLength = {4}
                    />
                    <Button
                        containerStyle={styles.containerIngresar}
                        buttonStyle = {styles.btnIngresar}
                        title="Siguiente"
                        titleStyle ={{fontSize:23,letterSpacing: -0.5750000000000001,}}
                        onPress = {()=>validarCodigo()}
                    />
                    <Loader
                        isVisible={isVisible}
                        text="Verificando código..."
                    />
            </View>
        </View>
        
    )
}

export default ValidarCodigo

const styles = StyleSheet.create({
    porFavor:{
        color: "rgba(151,158,181,1)",
        fontSize: 16,
        letterSpacing: -0.4
    },
    inputContaine : {
        width : "90%",
        height:49,
        borderWidth: 1,
        borderColor: "rgba(151,151,151,1)",
        borderRadius: 6,
        backgroundColor: "rgba(255,255,255,1)",
        marginBottom:20
    },
    inputStyle:{
        textAlign:"center",
        fontSize:18,
    },
    containerIngresar:{
        width:"90%",
        height:50,
        fontSize : 23,
        marginTop:50
    },
    btnIngresar : {
        borderRadius: 6,
        backgroundColor: "rgba(71,91,216,1)",
        fontSize : 23
    },
})
