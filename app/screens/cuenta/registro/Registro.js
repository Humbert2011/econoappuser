import React, { useState } from 'react'
import {Button,Input,CheckBox,Icon,Overlay} from 'react-native-elements';
import { StyleSheet, Text, View, ScrollView } from 'react-native'
import {useNavigation} from '@react-navigation/native';
import {size} from 'lodash';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import {validarEmail} from '../../../utils/validacion';
import Loader from '../../../components/Loader';
import Api from '../../../utils/Api';
import {url} from '../../../utils/config';
import {saveValue,getValueFor} from '../../../utils/localStorage';

const Registro = (props) => {
    const {route} = props;
    const {telefono,codigo_pais,tipoPersona,tokenTemReg} = route.params;
    const [parametros, setParametros] = useState({
        codigo_pais:codigo_pais,
        telefono: telefono,
        tipoPersona:tipoPersona,
        Nombre:"",
        apellidos:"",
        correo:"",
        password:"",
    })
    const [isVisible, setIsVisible] = useState(false)
    const [errors, setErrors] = useState({});
    const [verPassword, setVerPassword] = useState(false);
    const [terminos, setTerminos] = useState(false)
    const [showTycos, setShowTycos] = useState(false)
    const onchange = (e,tipo) =>{
        setParametros({...parametros,[tipo]:e.nativeEvent.text})
    };

    const registrar = async () => {
        setErrors({})
        if (!parametros.Nombre || !parametros.apellidos || !parametros.correo || !parametros.password ) {
            setErrors({password:"Todos los campos son obligatorios."}) 
        }else if (!validarEmail(parametros.correo)) {
            setErrors({correo:"Formato de correo inválido."})
        }else if (size(parametros.password) <= 5 ) {
            setErrors({password:"La contraseña debe tener 6 caracteres minimo."})
        }else if (!terminos) {
            setErrors({password:"Tiene que aceptar los términos y condiciones."})
        }else{
            setIsVisible(true);

            let tokenPush = null;
            await getValueFor('tokenPush').then(res=>tokenPush = res);
            let uri = `${url}persona/registerUser`;
            let api = new Api(uri,"POST",parametros,tokenTemReg);
            await api.call().then( async (res) => {
                console.log(uri,parametros,tokenTemReg)
                if (res.response) {
                    let parametrosLog = {
                        user: parametros.telefono,
                        password: parametros.password,
                        token:`${tokenPush}`,
                        plataforma:"Movil"
                    }
                    let uriLog = `${url}auth/loginUser`;
                    let apiLog = new Api(uriLog,"POST",parametrosLog);
                    setIsVisible(true);
                    await apiLog.call()
                            .then( async (resLog) => {
                                if (resLog.response) {
                                    await saveValue("token",resLog.result.token)
                                    await saveValue("id",resLog.result.id);
                                    setIsVisible(false);
                                    navegacion.navigate("espera");
                                } else {
                                    setIsVisible(false)
                                    setErrors({password:`${res.errors}`})
                                }
                            });
                } else {
                    console.log(res)
                    alert(`${res.errors}`)
                    setIsVisible(false)
                }
            });
        }
    }

    const navegacion = useNavigation();
    return (
        <KeyboardAwareScrollView style={{backgroundColor:"#fff"}} >
        <ScrollView style = {{backgroundColor : "#fff", height:"100%"}} >
            <View style={{margin:20}} >
                <Text style={{fontSize:24}} allowFontScaling={false}>Regístrate 👋</Text>
                <Text style={styles.peedesEmpezar}>
                    Puedes empezar a comprar tus productos después de registrarte.
                </Text>
            </View>
            <View style ={{justifyContent:"center",alignItems:"center",marginTop:20}} >
                    <Input
                        allowFontScaling={false}
                        placeholder = "Nombre"
                        style = {styles.inputStyle}
                        containerStyle = {styles.inputContaine}
                        selectionColor={'#3D5CA4'}
                        inputContainerStyle={{borderBottomWidth:0}}
                        // keyboardType= "numeric"
                        onChange={(e)=>onchange(e,"Nombre")}
                        errorMessage = {errors.Nombre}
                        // maxLength = {4}
                    />
                    <Input
                        allowFontScaling={false}
                        placeholder = "Apellidos"
                        style = {styles.inputStyle}
                        containerStyle = {styles.inputContaine}
                        inputContainerStyle={{borderBottomWidth:0}}
                        selectionColor={'#3D5CA4'}
                        // keyboardType= "numeric"
                        onChange={(e)=>onchange(e,"apellidos")}
                        errorMessage = {errors.apellidos}
                        // maxLength = {4}
                    />
                    <Input
                        allowFontScaling={false}
                        placeholder = "Correo Electrónico"
                        style = {styles.inputStyle}
                        containerStyle = {styles.inputContaine}
                        selectionColor={'#3D5CA4'}
                        inputContainerStyle={{borderBottomWidth:0}}
                        // keyboardType= "numeric"
                        onChange={(e)=>onchange(e,"correo")}
                        errorMessage = {errors.correo}
                        // maxLength = {4}
                    />
                   <Input
                    allowFontScaling={false}
                    placeholder = "Contraseña"
                    style = {styles.inputStyle}
                    containerStyle = {styles.inputContaine}
                    inputContainerStyle={{borderBottomWidth:0}}
                    selectionColor={'#3D5CA4'}
                    onChange={(e)=>onchange(e,"password")}
                    password={true}
                    secureTextEntry={verPassword ? false : true}
                    rightIcon={
                        <Icon
                            type="font-awesome" 
                            name = {verPassword ? "eye-slash" : "eye"}
                            size={18}
                            iconStyle = {{color:"gray"}}
                            onPress={()=>setVerPassword(!verPassword)}
                        />
                        }
                        errorMessage = {errors.password}
                    />
            </View>
            <View style={{flexDirection:'row',alignItems:'center',justifyContent:'flex-end',paddingRight:'5%'}} >
                <CheckBox
                    center
                    containerStyle = {{
                        backgroundColor:"#fff",
                        borderColor: "#fff",
                    }}
                    textStyle={{
                        fontStyle:"normal",
                        fontWeight:"normal",
                        color:"grey",
                    }}
                    // title = "Acepto los términos y condiciones."
                    checked = {terminos}
                    onPress = {()=>setTerminos(!terminos)}
                />
                <Text style={{color:'grey',marginLeft:-15}} onPress={()=>navegacion.navigate('terminosCondiciones')}>Acepto los términos y condiciones.</Text>
            </View>
            <View style ={{justifyContent:"center",alignItems:"center",marginTop:0}} >
                <Button
                    containerStyle={styles.containerBtn}
                    buttonStyle = {styles.btnStyle}
                    title="Registrarse"
                    titleStyle ={{fontSize:23,letterSpacing: -0.5750000000000001,}}
                    onPress = {()=>registrar()}
                />
                 <Text style={{color:"grey",marginTop:10}} onPress = { () => navegacion.navigate("login") } >
                    ¿Ya tienes cuenta? Inicia sesión
                </Text>
            </View>
            <Loader
                isVisible = {isVisible}
                text = "Cargando..."
            />
        </ScrollView>
        </KeyboardAwareScrollView>
    )
}

export default Registro

const styles = StyleSheet.create({
    peedesEmpezar:{
        color: "rgba(151,158,181,1)",
        fontSize: 16,
        letterSpacing: -0.4
    },
    inputContaine : {
        width : "90%",
        height:49,
        borderWidth: 1,
        borderColor: "rgba(151,151,151,1)",
        borderRadius: 6,
        backgroundColor: "rgba(255,255,255,1)",
        marginBottom:20
    },
    inputStyle:{
        textAlign:"center",
        fontSize:18,
    },
    containerBtn:{
        width:"90%",
        height:50,
        fontSize : 23,
        marginTop:5
    },
    btnStyle : {
        borderRadius: 6,
        backgroundColor: "rgba(71,91,216,1)",
        fontSize : 23,
    },
    
})
