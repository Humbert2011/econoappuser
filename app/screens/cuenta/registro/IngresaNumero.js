import React,{useState,useCallback} from 'react'
import { StyleSheet, Text, View } from 'react-native'
import {Input,Icon,Button} from 'react-native-elements';
import {useNavigation,useFocusEffect} from '@react-navigation/native';
import {size} from 'lodash';

import {saveValue,getValueFor} from '../../../utils/localStorage';
import Loader from '../../../components/Loader';
import Api from '../../../utils/Api';
import {url} from '../../../utils/config';

const IngresaNumero = () => {
    const [telTemp, setTelTem] = useState(null);

    useFocusEffect(
        useCallback(() => {
            getValueFor("telTemp").then(res =>{
                    setTelTem(res);
                } );
            },[]
        )
    )

    const navegacion = useNavigation();
    const [isVisible, setIsVisible] = useState(false);
    const [errors, setErrors] = useState({});
    const [parametros, setParametros] = useState({
        codigo_pais : "52",
        telefono: null,
        tipoPersona: "1"
    });

    const onchange = (e,tipo) =>{
        setParametros({...parametros,[tipo]:e.nativeEvent.text})
    };

    const validarNumero = async () =>{
        let numeros = parametros.telefono;
        let valoresAceptados = /^[0-9]+$/;
        setErrors({});
        if (!parametros.telefono) {
            setErrors({telefono:"El teléfono no puede ser nulo."});
        }else if (size(parametros.telefono) < 10){
            setErrors({telefono:"Hacen falta números en el teléfono."})
        }else if (!numeros.match(valoresAceptados)){
            setErrors({telefono:"No se permiten caracteres o letras."})
        }else{
            setIsVisible(true);
            let uri = `${url}auth/validateNumber`;
            console.log(parametros)
            let api = new Api(uri,"POST",parametros);
            await api.call().then( async (res)=>{
                if (!res.response) {
                    setIsVisible(false);
                    setErrors({telefono:res.errors});
                }else{
                    setIsVisible(false);
                    await saveValue("telTemp",parametros.telefono);
                    navegacion.navigate("validarCodigo",{telefono:parametros.telefono,codigo_pais:parametros.codigo_pais,tipoPersona:parametros.tipoPersona})
                }
            })
        }
            
    };

    return (
        <View style = {{backgroundColor : "#fff", height:"100%"}} >
             <View style={{margin:20, marginBottom:20}} >
                <Text style={{fontSize: 24}} >Ingresa tu número de teléfono móvil.</Text>
                
            </View>
            <View style ={{justifyContent:"center",alignItems:"center", marginTop:20 }} >
                <Input
                    placeholder = "México (+52)"
                    style = {styles.inputStyle}
                    containerStyle = {styles.inputContaine}
                    selectionColor={'#3D5CA4'}
                    inputContainerStyle={{borderBottomWidth:0}}
                    keyboardType = "numeric"
                    maxLength = {2}
                    disabled
                    // onChange={(e)=>onchange(e,"telefono")}
                />
                <Input
                    placeholder = "Teléfono"
                    style = {styles.inputStyle}
                    containerStyle = {styles.inputContaine}
                    inputContainerStyle={{borderBottomWidth:0}}
                    keyboardType= "numeric"
                    selectionColor={'#3D5CA4'}
                    onChange={(e)=>onchange(e,"telefono")}
                    errorMessage = {errors.telefono}
                    maxLength = {10}
                />
            </View>
            <View style ={{justifyContent:"center",alignItems:"center",marginTop:20}} >
            <Button
                containerStyle={styles.containerIngresar}
                buttonStyle = {styles.btnIngresar}
                title="Siguiente"
                titleStyle ={{fontSize:23,letterSpacing: -0.5750000000000001,}}
                onPress = {()=>validarNumero()}
            />
            {telTemp && <Text onPress = { () => { navegacion.navigate("validarCodigo",{telefono:telTemp,codigo_pais:parametros.codigo_pais,tipoPersona:parametros.tipoPersona}) } } style={{color:"grey", marginTop:15}} >Tengo un código de validación para: {telTemp} </Text> }
            </View>
            <Loader
                isVisible = {isVisible}
                text = {"Validando Número..."}
            />
        </View>
    )
}

export default IngresaNumero

const styles = StyleSheet.create({
    inputContaine : {
        width : "90%",
        height:49,
        borderWidth: 1,
        borderColor: "rgba(151,151,151,1)",
        borderRadius: 6,
        backgroundColor: "rgba(255,255,255,1)",
        marginBottom:20
    },
    inputStyle:{
        textAlign:"center",
        fontSize:18,
    },
    containerIngresar:{
        width:"90%",
        height:50,
        fontSize : 23
    },
    btnIngresar : {
        borderRadius: 6,
        backgroundColor: "rgba(71,91,216,1)",
        fontSize : 23
    },
})
