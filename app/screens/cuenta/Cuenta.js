import React, { useCallback } from 'react'
import { StyleSheet, Text, View, ScrollView, Alert } from 'react-native'
import { Button, Avatar, ListItem, Icon } from 'react-native-elements'
import { useFocusEffect, useNavigation } from '@react-navigation/native';

import { deleteValue, getValueFor } from '../../utils/localStorage';

const Cuenta = (props) => {
  const { setLogin } = props;
  const navegacion = useNavigation();

  const cerrarSesion = () => {
    Alert.alert(
      "Cerrar sesión.",
      "¿Está seguro de cerrar sesión?",
      [
        {
          text: "Cancelar",
          style: "cancel"
        },
        {
          text: "Aceptar",
          onPress: () => {
            deleteValue("token");
            deleteValue("id");
            setLogin(null);;
          }
        }
      ],
      {
        cancelable: false
      }
    )

  }

  useFocusEffect(
    useCallback(() => {
      (async () => {
        let move = await getValueFor('movePedidos');
        await deleteValue('movePedidos');
        move == 1 && navegacion.navigate('misPedidos');
      })()
    }, [])
  )

  const listCuenta = [
    {
      title: 'Perfil',
      onPress: () => navegacion.navigate('perfil')
    },
    {
      title: 'Mis pedidos',
      onPress: () => navegacion.navigate('misPedidos')
    },
    {
      title: 'Direcciones',
      onPress: () => navegacion.navigate('direcciones')
    },
    {
      title: "Métodos de Pago",
      onPress: () => navegacion.navigate('metodoPago')
    }
  ]

  const listAyuda = [
    {
      title: 'Términos y Condiciones',
      onPress: () => navegacion.navigate('terminosCondiciones')
    },
    {
      title: 'Contáctanos',
      onPress: () => navegacion.navigate('contactanos')
    }
  ];

  return (
    <ScrollView style={{ backgroundColor: "#fff" }} >
      <View style={{ justifyContent: "center", alignItems: "center", zIndex: 0, marginTop: 0 }} >
        <Avatar
          containerStyle={{ zIndex: 1000, marginTop: 10 }}
          size={100}
          rounded
          source={require('../../../assets/perfil.png')}
        >
          {/* <Avatar.Accessory
          onPress={()=>console.log("cambiar IMg")}
            size={29}
          /> */}
        </Avatar>
      </View>

      <Text style={{ color: "grey", marginTop: 20, marginLeft: 17, fontSize: 17 }} >Mi Cuenta</Text>

      <View style={{ marginBottom: 0 }} >
        {
          listCuenta.map((item, i) => (
            <ListItem key={i} onPress={item.onPress} >
              {/* <Icon name={item.icon} /> */}
              <ListItem.Content>
                <ListItem.Title style={{ fontSize: 21 }} >{item.title}</ListItem.Title>
              </ListItem.Content>
              <ListItem.Chevron iconStyle={{ fontSize: 25 }} />
            </ListItem>
          ))
        }
      </View>
      <Text style={{ color: "grey", marginLeft: 17, fontSize: 17 }} >Ayuda</Text>
      <View style={{ marginBottom: 0 }} >
        {
          listAyuda.map((item, i) => (
            <ListItem key={i} onPress={item.onPress} >
              {/* <Icon name={item.icon} /> */}
              <ListItem.Content>
                <ListItem.Title style={{ fontSize: 21 }} >{item.title}</ListItem.Title>
              </ListItem.Content>
              <ListItem.Chevron iconStyle={{ fontSize: 25 }} />
            </ListItem>
          ))
        }
      </View>
      <View style={
        {
          justifyContent: "center",
          alignItems: "center"
        }
      } >
        <Button
          title="Cerrar Sesión"
          onPress={() => cerrarSesion()}
          containerStyle={styles.containerIngresar}
          buttonStyle={styles.btnIngresar}
        />
      </View>
    </ScrollView>
  )
}

export default Cuenta

const styles = StyleSheet.create({

  containerIngresar: {
    width: "90%",
    height: 100,
    fontSize: 23
  },
  btnIngresar: {
    borderRadius: 6,
    backgroundColor: "#FF6719",
    fontSize: 23,
    marginTop: 50
  },

})
