import React, {useState,useEffect,useCallback} from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { useFocusEffect } from '@react-navigation/native';

import {getValueFor} from '../../utils/localStorage';
import Loader from '../../components/Loader';

import Invitado from './Invitado';
import Cuenta from './Cuenta';


const CuentaEspera = () => {
    const [login, setLogin] = useState(null);
    // se dispara cada que se entra al componente
    useFocusEffect(
        useCallback(() => {
                getValueFor("token").then((token)=>{
                    (token === false) ? setLogin(false) : setLogin(true)
                })
            },[login]
        )
    )

    if(login === null) return <Loader isVisible={true} text={"Cargando..."} />

    return login ? <Cuenta setLogin ={setLogin} /> : <Invitado/>
}

export default CuentaEspera

const styles = StyleSheet.create({})
