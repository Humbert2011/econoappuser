import React, { useState, useCallback } from 'react';
import { Image, ScrollView, StyleSheet, Text, View } from 'react-native';
import { ListItem, Input, Divider } from 'react-native-elements';
import { useFocusEffect } from '@react-navigation/native';
import { size } from 'lodash';

import { Shadow } from 'react-native-shadow-2';
import img from '../../../../assets/s.jpg'
import efectivo from '../../../../assets/efectivo.png'

import Api from '../../../utils/Api'
import { url } from '../../../utils/config';
import { getValueFor } from '../../../utils/localStorage';
import Loader from '../../../components/Loader';

export default function DetallePedidos({ route }) {
  const { params } = route;
  const { idpedido } = params;
  const [pedido, setPedido] = useState(null);
  const [producto, setProducto] = useState(null);
  const [totalVenta, setTotalVenta] = useState(0);

  useFocusEffect(
    useCallback(() => {
      (
        async () => {
          let token = await getValueFor('token');
          let uri = `${url}pedido/listardetapedido/${idpedido}`;
          let api = new Api(uri, "GET", null, token);
          api.call().then(res => {
            if (res.response) {
              //console.log(res);
              setPedido(res.result);
              setProducto(res.result.productos);
              let total = parseFloat(res.result.total);
              setTotalVenta(total.toFixed(2));
            }
          });
        })()
    }, [])
  )

  const list2 = [
    {
      pay: 'Pago en efectivo',
    }
  ]

  return (
    <View style={{ backgroundColor: 'white', height: '100%', width: '100%' }}>
      {
        !pedido ?
          <Loader isVisible={true} />
          : (
            <ScrollView>
              <View style={styles.firstContainer}>
                <Shadow>
                  <Image source={img} style={styles.logoView} />
                </Shadow>
                <Text style={styles.title} allowFontScaling={false}>Econosuper{'\n'}Suc. Huauchinango</Text>
              </View>

              <View style={styles.secondContainer}>
                <Image source={require('../../../../assets/no-image.png')} style={styles.photo} />
                <Text style={styles.text} allowFontScaling={false}> Llevado por: {'\n'} {pedido.data.Repartidor} </Text>
              </View>

              <View style={{ paddingLeft: '7%', padding: '5.85%', borderBottomColor: '#E3E0DE', borderBottomWidth: 6 }}>
                <Text style={{ fontSize: 18.5, fontWeight: 'bold', textAlign: 'center', paddingBottom: '7%' }} allowFontScaling={false}>Estatus del pedido </Text>
                {
                  <ListItem.Content >
                    <ListItem.Title style={{ fontSize: 19 }} allowFontScaling={false}>{pedido.data.status} </ListItem.Title>
                    {/* <ListItem.Subtitle style={{ fontSize: 19, color: '#959D9D' }} allowFontScaling={false}>Estamos colectando todos tus productos</ListItem.Subtitle> */}
                  </ListItem.Content>
                }
              </View>
              {
                  pedido.data.status === 'Cancelado' && (
                    <View style={{ borderBottomColor: '#E3E0DE', borderBottomWidth: 6, padding: '5.85%', paddingLeft: '7%' }}>
                          <View>
                            <Text style={{ fontSize: 20, fontWeight: 'bold', textAlign: 'center' }} allowFontScaling={false}>Nota de cancelación {'\n'} </Text>
                            <Text style={{ fontSize: 19 }} allowFontScaling={false}>{pedido.data.motivo}</Text>
                          </View>
                    </View>
                  ) 
              }
              <View style={{ borderBottomColor: '#E3E0DE', borderBottomWidth: 6, padding: '5.85%', paddingLeft: '7%' }}>
                <Text style={{ fontSize: 20, fontWeight: 'bold', textAlign: 'center' }} allowFontScaling={false}>Nota {'\n'} </Text>
                <Text style={{ fontSize: 19 }} allowFontScaling={false}>{pedido.data.nota}</Text>
              </View>

              <Text style={{ fontSize: 22, fontWeight: 'bold', paddingLeft: '7%', padding: '5.85%' }} allowFontScaling={false}>Tu lista de productos </Text>
              <View style={{ paddingLeft: '7%', borderBottomColor: '#E3E0DE', borderBottomWidth: 6 }}>
                {
                  !producto ? (
                    <Text allowFontScaling={false}>Sin productos</Text>
                  ) : (
                    size(producto) > 0 ?
                      (
                        producto.map((producto, i) => (
                          <ListItem key={i} bottomDivider>
                            <Image source={{ uri: producto.urlImg }} style={styles.productsStyles} />
                            <ListItem.Content>
                              <ListItem.Title style={{ fontSize: 18, fontWeight: 'bold' }} allowFontScaling={false}> {producto.nombre} </ListItem.Title>
                              <ListItem.Subtitle style={{ fontSize: 16 }} allowFontScaling={false}> {producto.cantidad} Kilogramos </ListItem.Subtitle>
                              <ListItem.Subtitle style={{ fontSize: 18, color: 'black' }} allowFontScaling={false}> ${producto.costo} </ListItem.Subtitle>
                            </ListItem.Content>
                          </ListItem>
                        ))
                      )
                      : (
                        console.log('no hay pedidos')
                      )
                  )
                }
              </View>

              <View style={{ paddingLeft: '7%', padding: '5%', borderBottomColor: '#E3E0DE', borderBottomWidth: 6, flexDirection: 'row' }}>
                <Text style={{ fontSize: 19, paddingRight: '15%', height: '100%', width: '64%', textAlignVertical: 'center' }} allowFontScaling={false}>Dirección de entrega </Text>
                <Text style={{ fontSize: 16, fontWeight: 'bold', textAlign: 'right', height: '100%', width: '35%' }} allowFontScaling={false}>{pedido.data.Direccion}</Text>
              </View>

              <View style={{ paddingLeft: '7%', padding: '5%', borderBottomColor: '#E3E0DE', borderBottomWidth: 6, flexDirection: 'row' }}>
                <Text style={{ fontSize: 19, width: '70%', textAlignVertical: 'center' }} allowFontScaling={false}> Total a pagar</Text>
                <ListItem>
                  <ListItem.Content >
                    <ListItem.Title style={{ fontSize: 19, fontWeight: 'bold', width: 100 }} allowFontScaling={false}>${totalVenta}</ListItem.Title>
                  </ListItem.Content>
                </ListItem>
              </View>

              <View style={{ paddingLeft: '5%', paddingBottom: '10%' }}>
                {
                  list2.map((l, i) => (
                    <ListItem key={i} >
                      <Image source={efectivo} style={styles.moneyIcon} />
                      <ListItem.Content>
                        <ListItem.Title style={{ fontSize: 18, width: '96.5%', padding: '10%' }} allowFontScaling={false}> {l.pay} </ListItem.Title>
                      </ListItem.Content>
                      {/*// <ListItem.Content>
                      //   <TouchableOpacity style={{ marginLeft: '42%' }}>
                      //     <Text style={{ fontSize: 18, color: '#0A78B7' }} allowFontScaling={false}>Cambiar</Text>
                      //   </TouchableOpacity>
                      // </ListItem.Content>*/}

                    </ListItem>
                  ))
                }
              </View>
            </ScrollView>
          )
      }
    </View>
  );
}

const styles = StyleSheet.create({
  logoView: {
    height: 55,
    width: 55,
    borderRadius: 50,
    backgroundColor: 'gray',
  },
  photo: {
    height: 60,
    width: 60,
    borderRadius: 50,
  },
  firstContainer: {
    flexDirection: 'row',
    paddingLeft: '7%',
    paddingVertical: '5.85%'
  },
  title: {
    color: 'black',
    fontSize: 25,
    fontWeight: 'bold',
    paddingStart: '6%'
  },
  text: {
    fontSize: 18,
    paddingStart: '2.5%',
    width: '60%',
    textAlign: 'center'
  },
  secondContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    paddingStart: '31%',
    height: '8%',
    borderBottomColor: '#E3E0DE',
    borderBottomWidth: 1
  },
  productsStyles: {
    width: 50,
    height: 50
  },
  moneyIcon: {
    width: 35,
    height: 20
  }
})