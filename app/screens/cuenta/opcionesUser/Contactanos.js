import React from 'react'
import { View, Text } from 'react-native'
import { Icon } from 'react-native-elements'

const Contactanos = () => {

    return (
        <View style={{ flex:1, alignItems: 'center', justifyContent: 'center',backgroundColor: '#fff'}}>

            <View style={{ flexDirection: 'row', bottom:10 }}>
                <Icon name='phone' type='font-awesome' size={30} />
                <Text style={{ fontWeight: 'bold', fontSize: 18 }}>  776 135 2908</Text>
            </View>

            <View style={{ flexDirection: 'row' }}>
                <Icon name='email' size={30} />
                <Text style={{ fontWeight: 'bold', fontSize: 18 }}> eco@gmail.com</Text>
            </View>

        </View>
    )
}
export default Contactanos

