import React, { useState, useCallback } from 'react'
import { StyleSheet, Text, ScrollView, View, TextInput, TouchableOpacity, Image } from 'react-native'
import { Tab, TabView, ListItem, colors } from 'react-native-elements';
// import img from '../../../../assets/s.jpg'
import { useFocusEffect } from '@react-navigation/native';
import { size } from 'lodash';

import LoaderLineal from '../../../components/LoaderLineal'
import Api from '../../../utils/Api';
import { url } from '../../../utils/config';
import { getValueFor } from '../../../utils/localStorage';
import ListaPedidos from '../../../components/misPedidos/ListaPedidos'

const MisPedidos = () => {
    const [index, setIndex] = useState(0);
    const [offset, setoffset] = useState(0);
    const [limit, setLimit] = useState(10);
    const [activos, setactivos] = useState(null);
    const [inactivos, setinactivos] = useState(null);
    const [total, setTotal] = useState(0);
    const [isLoading, setIsLoading] = useState(false);

    useFocusEffect(
        useCallback(() => {
            (async () => {
                setoffset(0);
                let tipo = 'activos'
                index == 0 ? tipo = 'activos' : tipo = 'inactivos';
                let token = await getValueFor('token');
                let id = await getValueFor('id');
                let uri = `${url}pedido/listpedidosactivoinactivo/${id}/${tipo}/0`;
                let api = new Api(uri, "GET", null, token);

                api.call().then(res => {
                    if (res.response) {
                        setTotal(res.result.total)
                        tipo == 'activos' ? (setactivos(res.result.pedidos)) : (setinactivos(res.result.pedidos));
                        console.log(res.result.pedidos)
                    }
                });
            })()
        }, [index])
    )

    const morePedidos = async () => {
        console.log(total, offset)
        if (total < offset) {
            setIsLoading(true);
            return false
        }

        let pag = offset + 10;
        await setoffset(offset + 10);
        let tipo = 'activos';
        index == 0 ? tipo = 'activos' : tipo = 'inactivos';
        let token = await getValueFor('token');
        let id = await getValueFor('id');
        let uri = `${url}pedido/listpedidosactivoinactivo/${id}/${tipo}/${pag}`;
        console.log(uri)
        let api = new Api(uri, "GET", null, token);

        api.call().then(res => {
            console.log(res)
            setIsLoading(false)
            if (res.response) {
                setTotal(res.result.total)
                let data = res.result.pedidos;
                tipo == 'activos' ? (setactivos([...activos, ...data])) : (setinactivos([...inactivos, ...data]));
            }
        });

    }

    return (
        <View style={styles.container}>

            <Tab value={index} onChange={setIndex} indicatorStyle={{ backgroundColor: "#FE430E", height: 4 }}>
                <Tab.Item containerStyle={{ backgroundColor: "#fff" }} titleStyle={{ color: "black", fontSize: 16, fontWeight: 'bold', textTransform: 'none' }} title="Activos" />
                <Tab.Item containerStyle={{ backgroundColor: "#fff" }} titleStyle={{ color: "black", fontSize: 16, fontWeight: 'bold', textTransform: 'none' }} title="Pedidos anteriores" />
            </Tab>

            <TabView value={index} onChange={setIndex} >
                <TabView.Item onMoveShouldSetResponder={(e) => e.stopPropagation()} style={styles.tab}>
                    <View style={styles.list}>
                        {
                            !activos ? (
                                <View style={{ marginTop: '55%' }} >
                                    <LoaderLineal />
                                </View>
                            ) : (
                                size(activos) > 0 ? (
                                    <ListaPedidos
                                        pedidos={activos}
                                        morePedidos={morePedidos}
                                        isLoading={isLoading}
                                    />
                                )
                                    : (
                                        <View style={{ marginTop: '55%', alignItems: 'center', justifyContent: 'center' }}>
                                            <Text style={{ color: '#CFCFCF', fontSize: 16 }} >
                                                No tienes pedidos activos.
                                            </Text>
                                        </View>
                                    )
                            )
                        }
                    </View>
                </TabView.Item>

                <TabView.Item onMoveShouldSetResponder={(e) => e.stopPropagation()} style={styles.tab}>
                    <View style={styles.list}>
                        {
                            !inactivos ? (
                                <View style={{ marginTop: '55%' }} >
                                    <LoaderLineal />
                                </View>
                            ) : (
                                size(inactivos) > 0 ? (
                                    <ListaPedidos pedidos={inactivos}
                                        morePedidos={morePedidos}
                                        isLoading={isLoading}
                                    />
                                )
                                    : (
                                        <View style={{ marginTop: '55%', alignItems: 'center', justifyContent: 'center' }}>
                                            <Text style={{ color: '#CFCFCF', fontSize: 16 }} >
                                                No tienes pedidos anteriores.
                                            </Text>
                                        </View>
                                    )
                            )
                        }
                    </View>
                </TabView.Item>
            </TabView>

        </View>
    )
}

export default MisPedidos

const styles = StyleSheet.create({
    container: {
        width: "100%",
        height: "100%",
        backgroundColor: "#fff",
    },
    tab: {
        width: '100%',
        height: '100%',
    },
    list: {
        borderColor: colors.greyOutline,
        width: "100%",
        height: "100%"
    },
    button: {
        marginLeft: '20%',
        alignItems: "center",
        backgroundColor: "#FC6915",
        padding: 8,
        borderRadius: 10
    },
    fontButton: {
        color: 'white',
    }
})
