import React,{useEffect,useState} from 'react'
import { StyleSheet, View, ScrollView } from 'react-native'
import { Avatar, Input ,Button } from 'react-native-elements';

import {getValueFor} from '../../../utils//localStorage';
import Api from '../../../utils/Api';
import {url} from '../../../utils/config';
import Loader from '../../../components/Loader';

const Perfil = () => {
    const [parametros, setParametros] = useState({});
    const [infoUser, setInfoUser] = useState(null)
    const [tokenAuth, setTokenAuth] = useState(null);
    const [isVisible, setIsVisible] = useState(false);
   
    useEffect(() => {
        getValueFor("token").then(res=>{
            setTokenAuth(res);
            let token = atob(res);
            let tokenDecode = JSON.parse(token);
            setInfoUser(tokenDecode.data)
        });
    }, []);

    const onchange = (e,tipo) =>{
        setParametros({...parametros,[tipo]:e.nativeEvent.text})
    }

    const actualizarInfo = async () => {
        // console.log(tokenAuth)
        if (Object.keys(parametros).length === 0 ) {
            alert("No hay cambios en los datos.")
        }else{
            setIsVisible(true);
            let uri = `${url}persona/actualizar/${infoUser.idpersona}`
            console.log(parametros,uri)
            let api = new Api(uri,"PUT",parametros,tokenAuth);
            await  api.call().then(res=>{
                console.log(res)
                if (res.response) {
                    setIsVisible(false);
                    alert("Cierre e inicie sesión para ver los cambios...");
                }else{
                    setIsVisible(false);
                    alert("Error intentelo de nuevo más tarde.")
                }
            })
           
        }
    }

    if (!infoUser) return <Loader isVisible = {true} text ="Cargando..." />
    
    return (
        <ScrollView style={{height:'100%',width:'100%',backgroundColor:'#fff'}} >
            <View>
                <Avatar
                    containerStyle = {{zIndex:1000,marginTop:10,marginLeft:17}}
                    size="large"
                    rounded
                    source={require('../../../../assets/perfil.png')}
                    >
                    {/* <Avatar.Accessory
                        onPress={()=>console.log("cambiar Img")}
                        size={29}
                    /> */}
                </Avatar>
            </View>
            <View style={{marginTop:30,marginLeft:17}} >
                <Input 
                    label="Nombre"
                    defaultValue = {infoUser.Nombre}
                    selectionColor={'#3D5CA4'}
                    labelStyle={{fontSize:16,fontWeight:"normal",color:"rgba(123,127,135,1)" }}
                    inputContainerStyle={{borderBottomWidth:0}}
                    onChange={(e)=>onchange(e,"Nombre")}
                />
                <Input
                    label="Apellidos"
                    defaultValue = {infoUser.apellidos}
                    selectionColor={'#3D5CA4'}
                    labelStyle={{fontSize:16,fontWeight:"normal",color:"rgba(123,127,135,1)" }}
                    inputContainerStyle={{borderBottomWidth:0}}
                    onChange={(e)=>onchange(e,"apellidos")}
                />
                <Input
                    label="Correo Electrónico"
                    disabled
                    value = {infoUser.correo}
                    labelStyle={{fontSize:16,fontWeight:"normal",color:"rgba(123,127,135,1)" }}
                    inputContainerStyle={{borderBottomWidth:0}}
                />
                <Input
                    label="Número de Teléfono"
                    disabled
                    value = {infoUser.telefono} 
                    labelStyle={{fontSize:16,fontWeight:"normal",color:"rgba(123,127,135,1)" }}
                    inputContainerStyle={{borderBottomWidth:0}}
                />
            </View>
            <View style={{justifyContent:"center",alignItems:"center"}} >
                    <Button 
                        containerStyle = {styles.containerIngresar}
                        title="Guardar Cambios" 
                        buttonStyle = {styles.btnIngresar}
                        onPress = {()=> actualizarInfo()}
                    />
            </View>
            <Loader
                isVisible = {isVisible}
                text = "Cargando..."
            />
        </ScrollView>
    )
}

export default Perfil

const styles = StyleSheet.create({
    btnIngresar : {
        borderRadius: 6,
        backgroundColor: '#3D5CA4',
        fontSize : 23
    },
    containerIngresar:{
        width:"90%",
        height:50,
        fontSize : 23
    },
})
