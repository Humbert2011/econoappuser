import React, { useState } from 'react'
import { StyleSheet, Text, View, ScrollView } from 'react-native'
import { Tab, TabView } from 'react-native-elements'

const TerminosCondiciones = () => {

    const [index, setIndex] = useState(0);
    const changeColor = useState(1);
    console.log(index);
    return (
        <View style={styles.container} >
            <Tab value={index} onChange={setIndex} indicatorStyle={{ backgroundColor: "#FE430E", height: 4 }}>
                <Tab.Item containerStyle={{ backgroundColor: "#fff" }} titleStyle={index == 0 ? styles.activeTitle : styles.inactiveTitle} title="Términos y Condiciones" />
                <Tab.Item containerStyle={{ backgroundColor: "#fff" }} titleStyle={index == 1 ? styles.activeTitle : styles.inactiveTitle} title="Aviso Legal" />
            </Tab>
            <TabView value={index} onChange={setIndex} >
                <TabView.Item onMoveShouldSetResponder={(e) => e.stopPropagation()} style={styles.tab}>
                    <ScrollView style={{ width: '100%', height: '100%', zIndex: 1000 }} >
                        <Text style={styles.text} allowFontScaling={false}>
                            Bienvenido a la app y servicios digitales de compras en línea de Econosuper,
                            este sitio/app es propiedad de Econosuper de la Sierra S.A. de C.V. con RFC
                            ESI020716BLA, ésta última con domicilio fiscal en: Hidalgo 3, Colonia Centro,
                            Huauchinango, Puebla, C.P. 73170.
                            {'\n'}{'\n'}
                            Por favor, revisa los siguientes términos y condiciones que rigen el uso del
                            sitio web y la app. Al favorecernos con su aceptación, significa que has leído,
                            entendido y aceptas en su totalidad los términos y condiciones del mismo y
                            acordado que los presentes términos y condiciones son legalmente vinculantes
                            para el “Usuario”.
                            {'\n'}{'\n'}
                            El Usuario se sujetará a los presentes términos y condiciones de Uso de  la app
                            de Econosuper. Antes de utilizar Econosuper, por favor lea los presentes Términos y
                            Condiciones con atención (especialmente las partes resaltadas en negritas).
                            Tenga en cuenta que el Propietario no podrá acceder, ni tener pleno acceso a los
                            Servicios hasta que no haya proporcionado todos los documentos e información que
                            se requieren, aprobados por Econosuper y se obligue a sujetarse a los Términos y
                            Condiciones.
                            {'\n'}
                            Los presentes Términos y Condiciones son aplicables para todos los usuarios o
                            repartidores de Econosuper y están sujetos a modificación en cualquier momento
                            como resultado de ajustes a la política comercial. El Usuario debe visitar
                            frecuentemente la aplicación y sitio web de Econosuper para mantenerse al tanto
                            de los términos vigentes. Las notificaciones, términos u otros requerimientos
                            expresamente especificados o integrados en la página web y/o en la aplicación
                            en Econosuper, formarán parte integral de los presentes Términos y Condiciones,
                            ciertas partes de los términos podrán ser sustituidas por avisos legales,
                            términos o requerimientos más actualizados expresamente especificados o
                            integrados en la página web y/o aplicación en Econosuper.
                            Al aceptar los Términos y Condiciones, el Usuario también habrá leído con
                            atención y aceptado dichos términos sustituidos o referidos. Si el Usuario
                            sigue utilizando los servicios de la Compañía, se considerará que el Usuario
                            ha aceptado los Términos y Condiciones actualizados o el Usuario dejará de
                            utilizar Econosuper inmediatamente.
                            {'\n'} {'\n'}
                            Los presentes Términos y Condiciones expresamente dejan sin efecto cualquier
                            acuerdo o arreglo previo que la Compañía haya celebrado con el Usuario para
                            efectos de utilizar Econosuper y los Servicios. Econosuper podrá dar por
                            terminados los presentes Términos y Condiciones o cualquier Servicio con
                            respecto al Usuario inmediatamente, o en general, dejar de ofrecer o negar
                            el acceso a los Servicios o a cualquier parte de los mismos, si Econosuper
                            considera que el Usuario ha incurrido en cualquier incumplimiento.
                            {'\n'} {'\n'}
                            Econosuper se compromete a no transferir a un tercero sus datos personales,
                            asimismo, para garantizar la protección y confidencialidad de los
                            Datos Personales ha establecido mecanismos de seguridad administrativos,
                            técnicos o físicos. Tampoco recabará datos personales sensibles para poder
                            llevar a cabo las finalidades que se mencionan en los presentes
                            Términos y Condiciones.
                            {'\n'} {'\n'}
                            Asimismo, Econosuper se reserva el derecho de negarse a prestar los
                            Servicios al Usuario o negar al Usuario el uso de páginas web, servicios o
                            aplicaciones de Econosuper sin causa alguna.
                            {'\n'}{'\n'}{'\n'}
                            <Text style={styles.title}>
                                - REGISTRO A LA APLICACIÓN COMO USUARIOS
                                {'\n'}{'\n'}
                            </Text>
                            ⚫︎ Para utilizar el servicio en línea de pedidos, el Usuario debe descargar
                            la app de Econosuper, instalarla en su dispositivo móvil y completar
                            exitosamente los procesos de registro.
                            {'\n'}
                            ⚫︎ El Usuario deberá reconocer que los datos proporcionados en el registro
                            son verdaderos, para en dado caso de olvido se pueda hacer una recuperación
                            de los datos.
                            {'\n'}{'\n'}
                            <Text style={styles.title}>
                                - USO DE LA APP
                                {'\n'}{'\n'}
                            </Text>
                            Los contenidos y servicios que ofrecemos están dirigidos a un público mayor
                            de 18 años. Por lo que queda bajo responsabilidad de los padres o tutores,
                            supervisar la conducta de los menores de edad que ingresen a los sitios.
                            Econosuper y filiales podrá cancelar o restringir tu cuenta en dado caso que
                            detecte algún uso indebido del sitio y de los servicios que se ofrezcan en el
                            mismo, entendiendo eso como uso indebido. Que en este caso sería como:
                            {'\n'}{'\n'}
                            ⚫︎ Utilizar los servicios para revender productos y cualquier otro uso comercial
                            de la app y sitios web de los contenidos.
                            {'\n'}
                            ⚫︎ La utilización de mecanismos o herramientas automatizadas o tecnología
                            similar cuya finalidad sea realizar la extracción, obtención o recopilación,
                            directa o indirecta, de cualquier información contenida en el sitio.
                            {'\n'}
                            ⚫︎ Cualquier intento de modificación, adaptación, traducción, o conversión de
                            los formatos o programas de cómputo de los Sitios web o de los contenidos de
                            los mismos.
                            {'\n'}
                            ⚫︎ Utilizar los códigos HTML a disposición de un tercero.
                            {'\n'}
                            ⚫︎ Copiar, imitar, replicar para su uso en servidores espejo, reproducir,
                            distribuir, publicar, descargar, mostrar o transmitir cualquier Contenido del
                            Sitio (incluyendo marcas registradas) en cualquier forma o por cualquier medio;
                            esta restricción incluye, pero no se limita a los siguientes medios:
                            medios electrónicos, mecánicos, medios de fotocopiado, grabación o
                            cualquier otro medio.
                            {'\n'}
                            ⚫︎ Acceder a datos no destinados al usuario o iniciar sesión en un servidor
                            o cuenta en la que el usuario no esté autorizado su acceso.
                            {'\n'}
                            ⚫︎ Intentar interferir con el servicio a cualquier usuario, huésped o red,
                            incluyendo, sin limitación, a través del envío de virus al Sitio,
                            sobrecarga, inundación, spam, bombardeo de correo o fallas.
                            {'\n'}
                            ⚫︎ Enviar correo no solicitado, incluyendo promociones y/o publicidad de
                            productos o servicios.
                            {'\n'}
                            ⚫︎ Intento o realización de actividades fraudulentas entre las que se encuentran
                            sin limitar, la falsificación de identidades o formas de pago.
                            {'\n'}{'\n'}
                            Toda la información de registro y facturación proporcionada deberá ser
                            verdadera y exacta. Proporcionar cualquier información falsa o inexacta
                            constituye el incumplimiento de estos TÉRMINOS Y CONDICIONES.
                            Al confirmar tu compra y finalizar el proceso de pago, estás de acuerdo en
                            aceptar y pagar por los artículos solicitados, así como en los datos de
                            facturación proporcionados a Econosuper y su sitio web.
                            {'\n'}
                            Las violaciones al sistema o red de seguridad pueden dar lugar a
                            responsabilidad civil o penal, Econosuper S.A de C.V investigará las
                            ocurrencias que puedan involucrar tales violaciones, así como cooperar
                            con las autoridades en la persecución de usuarios que infrinjan tales
                            violaciones. Haciendo uso de estos sitios, te obligas a no utilizar
                            ningún dispositivo, o software para interferir o intentar interferir
                            con el uso correcto de estos sitios o cualquier actividad llevada a cabo
                            en los mismos.
                            {'\n'}{'\n'}
                            <Text style={styles.title}>
                                - INFORMACIÓN SOBRE LOS PRODUCTOS
                                {'\n'}{'\n'}
                            </Text>
                            La información de los productos anunciados en el sitio de Econosuper es
                            exacta y apegada a las características de los mismos.
                            {'\n'}
                            Exhibimos con precisión los colores que aparecen de nuestros productos en
                            los sitios. Sin embargo, los colores actuales que ves dependerán de tu
                            pantalla, no podemos garantizar que ninguno de los colores mostrados en tu
                            pantalla sea exacto, te recordamos que las imágenes o fotografías que nos
                            proporcionan los proveedores, así como las de los “sellers” autorizados,
                            son con fines ilustrativos y te son expuestas de modo orientativo.
                            Te recomendamos que siempre verifiques las condiciones de compra antes de
                            realizar tu pedido, a fin de cerciorarte de los términos, condiciones y
                            restricciones que pudieren aplicar a cada producto.
                            {'\n'}
                            Todos los precios de los productos incluyen el IVA y demás impuestos
                            que pudieran corresponderles. Lo anterior, independientemente de los
                            gastos de envío que pudieran o no generarse. Estos montos se te indicarán
                            antes de levantar tu pedido, para que estés en posibilidad de aceptarlos.
                            {'\n'}
                            El formato en que se muestran los precios dentro del portal de Econosuper
                            puede verse modificado por las versiones de navegador, sistema operativo o
                            dispositivos para navegación de internet, por lo cual puede existir variaciones
                            en cuanto al uso de diversos dispositivos.
                            {'\n'}
                            De presentarse alguna inconsistencia o fallas al momento de publicar los
                            precios en el portal, se te informará de dicha situación, para que confirmes
                            si aún es tu deseo la adquisición del producto, o bien cancelar el pedido y
                            devolver el importe pagado. En caso de que no hayas finalizado tu compra,
                            se te notificará el cambio en precio y disponibilidad al ingresar al carrito
                            de compra.
                            {'\n'}{'\n'}
                            <Text style={styles.title}>
                                - GARANTIA
                                {'\n'}{'\n'}
                            </Text>
                            Econosuper busca que los artículos que se ofrecen estén respaldados con garantía
                            del fabricante, la cual siempre se apega a las disposiciones y términos
                            establecidos en Ley Federal de Protección al Consumidor.
                            La garantía se entregará con tu producto en donde se establecerán las
                            condiciones y mecanismos para hacerlas efectivas.
                            {'\n'}
                            El cumplimiento de las garantías de los productos, serán brindada
                            directamente por los “sellers” autorizados, la vigencia de dichas garantías
                            se refiere en la descripción de cada producto siendo emitida y validada
                            por los “sellers” de cada producto, por lo que no asume responsabilidad
                            alguna en torno a dichas garantías y el cliente se obliga a contactar
                            directamente al seller para hacer efectiva dicha garantía, no obstante,
                            realizará las acciones necesarias para proporcionar a los clientes que
                            así lo soliciten los datos de contacto para validar las respectivas
                            garantías, si fuera necesario.
                            {'\n'}
                            Es responsabilidad del cliente revisar la mercancía al momento de la
                            recepción. Si el empaque o producto se encuentra rayado, con golpes,
                            sucio, mojado o con otra forma de daño.
                            {'\n'}
                            Sugerimos:
                            {'\n'}
                            ⚫︎ No lo recibas y repórtelo al área de atención a clientes
                            {'\n'}
                            ⚫︎ Validaremos envío de sustituto o aplicar devolución.
                            {'\n'}
                            ⚫︎ Al recibir el artículo deberá firmar de recibido y de conformidad.
                            {'\n'}{'\n'}
                            <Text style={styles.title}>
                                - LÍMITES DE CANTIDAD Y DISPONIBILIDAD
                                {'\n'}{'\n'}
                            </Text>
                            Te informamos que, con la finalidad de beneficiar a una mayor cantidad
                            de clientes, la venta de algunos artículos de alta demanda podrá ser
                            limitada a determinadas piezas, esta situación te será comunicada antes
                            de realizar tu pedido y concretar tu compra. Igualmente el precio mínimo
                            para poder hacer un pedido es de 150$ para poder llevarse a domicilio.
                            {'\n'}
                            Los artículos están sujetos a disponibilidad, del cual el Cliente deberá
                            de verificar antes de procesar el pago. Lo anterior, es con la intención
                            de validar la existencia de los artículos y en caso de no estar disponible,
                            el sistema mostrará la siguiente leyenda “Sin productos disponibles para
                            tu dirección." El límite para poder hacer la entrega a domicilio es de
                            4km en la que se puede entregar el pedido.
                            {'\n'}{'\n'}
                            <Text style={styles.title}>
                                -ACEPTACIÓN DE PEDIDO
                                {'\n'}{'\n'}
                            </Text>
                            Una vez que realices tu pedido se te enviará notificación por medio de
                            la app y podrás dar seguimiento a dicho pedido, lo anterior no significa
                            que el pago ha sido procesado o autorizado por el banco o medios de pago
                            que elegiste, ni que el pedido ha sido confirmado.
                            Si el pago es autorizado o autenticado te enviaremos otro correo
                            electrónico con la confirmación del pedido y los detalles de la compra
                            o en su defecto se realizará el pago a contra-entrega del pedido.
                            {'\n'}{'\n'}
                            <Text style={styles.title}>
                                -DEVOLUCIONES
                                {'\n'}{'\n'}
                            </Text>
                            Si no estás satisfecho con el producto, puedes solicitar un devolución
                            o comprar otro producto con el mismo costo (no se aceptan productos ya
                            abiertos o con signo de haber intentado hacerlo, bebidas alcohólicas,
                            alimentos enlatados).
                            {'\n'}{'\n'}
                            <Text style={styles.title}>
                                -CANCELACIONES
                                {'\n'}{'\n'}
                            </Text>
                            Tu pedido comienza a procesarse inmediatamente después del clic en Comprar.
                            Una vez que el pedido sea confirmado por nuestro sistema, no será posible
                            cancelarse y deberá tramitarse como una devolución.
                            Para realizar una devolución de tu pedido solicítala en el establecimiento,
                            deberás tener a la mano tu número de pedido y en caso de una cancelación
                            parcial, el detalle de los artículos a cancelar.
                            {'\n'}{'\n'}
                            <Text style={styles.title}>
                                -DISPOSICIONES
                                {'\n'}{'\n'}
                            </Text>
                            ⚫︎ Si se declara la nulidad de ciertos términos de los ya presentes Términos
                            y Condiciones, pero los demás términos pueden seguir siendo válidos y su
                            exigibilidad no se ve afectada, Econosuper determinará si continuará
                            cumpliendo o no con tales otros términos.
                            {'\n'}
                            ⚫︎ Econosuper podrá entregar una notificación publicando una notificación
                            general en su sitio web y/o en Econosuper o enviando un correo electrónico
                            o mensaje de texto a la dirección de correo electrónico o número de teléfono
                            móvil registrado en la información de la cuenta del Usuario.
                            Las notificaciones, que podrán publicarse de tiempo en tiempo, constituirán
                            parte de los presentes Términos y Condiciones.
                            {'\n'}
                            ⚫︎ El Usuario no cederá ninguno de los derechos conforme a los presentes
                            Términos y Condiciones sin el previo consentimiento por escrito de
                            Econosuper.
                            {'\n'}{'\n'}
                            <Text style={styles.title}>
                                -OTROS TÉRMINOS
                                {'\n'}{'\n'}
                            </Text>
                            Los Términos y Condiciones hacen referencia a los siguientes términos
                            adicionales, los cuales también serán aplicables al uso de la app,
                            contenido, productos, Servicios y aplicaciones de la Compañía por el
                            Usuario, mismos que, al utilizarlos, el Usuario se obliga a cumplir:
                            {'\n'}
                            ⚫︎ Aviso de Privacidad de Econosuper establece los términos conforme a
                            los cuales los datos personales y otra información recopilada o
                            proporcionada por el Usuario deberá ser tratada.
                            {'\n'}{'\n'}
                            <Text style={styles.title}>
                                - Ley Aplicable
                                {'\n'}{'\n'}
                            </Text>
                            Los presentes Términos y Condiciones se regirán por las leyes aplicables
                            en México. Cualquier conflicto, reclamación o controversia derivada de o
                            relacionada con el incumplimiento, terminación, cumplimiento,
                            interpretación o validez de los presentes Términos y Condiciones o el uso
                            de nuestro sitio web o Econosuper, se someterá a la jurisdicción de un
                            tribunal competente de la Ciudad de Huauchinango, México y las partes en
                            este acto expresa e irrevocablemente renuncian a cualquier otra
                            jurisdicción que pueda corresponderles en virtud de sus domicilios
                            respectivos, presentes o futuros.
                            {'\n'}{'\n'}
                            <Text style={styles.title}>
                                - Subsistencia
                                {'\n'}{'\n'}
                            </Text>
                            Aún cuando los presentes Términos y Condiciones den por terminados o
                            anulen, las disposiciones relacionadas con la responsabilidad por
                            incumplimiento, propiedad industrial, obligación de confidencialidad del
                            Pasajero, ley aplicable y jurisdicción subsistirán.
                        </Text>
                    </ScrollView>
                </TabView.Item>

                <TabView.Item style={styles.tab}>
                    <ScrollView>
                        <Text style={styles.text} allowFontScaling={false}>
                            <Text style={styles.title}>
                                INTRODUCCIÓN
                                {'\n'}
                            </Text>
                            Cuando utiliza la app de Econosuper, nos confía su información. Por ello
                            valoramos la privacidad de su información. Nos comprometemos a no defraudar
                            su confianza. Por ello, lo primero que queremos hacer es el ayudarle a
                            entender nuestras prácticas de privacidad.
                            {'\n'}{'\n'}
                            Esta política de privacidad describe la información que recopilamos,
                            como la utilizamos y compartimos, tiene por objeto proporcionarle una
                            visión clara de cómo usamos la información personal que Usted nos
                            proporcionó y así mismo nuestros esfuerzos por protegerla.
                            {'\n'}{'\n'}
                            <Text style={styles.title}>
                                RECOPILACIÓN DE DATOS Y APLICACIONES
                                {'\n'}
                            </Text>
                            Esta política describe la forma en la que Econosuper recopila y utilizan la
                            información personal para proporcionar servicios. Esta política se aplica
                            a todos los usuarios de nuestras apps, sitios web, herramientas y otros
                            servicios en Huauchinango, Xicotepec, Tenango, Necaxa y alrededores, a
                            menos que se aplique una política de privacidad independiente.
                            Esta política se aplica a:
                            {'\n'}
                            <Text style={styles.title}>
                                ▪ Usuarios:
                            </Text>
                            {'\b'}Usuarios que solicitan o reciben un servicio de transporte.
                            {'\n'}
                            Todas las personas sujetas a esta política se denominan “usuarios” en el
                            contexto de la misma.
                            {'\n'}
                            Las prácticas descritas en la presente política están sujetas a la
                            legislación vigente en su lugar de aplicación. Esto significa que solo
                            aplicaremos las prácticas descritas en esta política en un país o una
                            región concreta si lo permiten las leyes de esos lugares.
                            Contacte con nosotros si tiene preguntas sobre nuestras prácticas en su
                            región.
                            {'\n'}{'\n'}
                            <Text style={styles.title}>
                                Información que recopilamos
                                {'\n'}
                            </Text>
                            La siguiente información es recopilada por Econosuper o en su nombre:
                            {'\n'}{'\n'}
                            <Text style={styles.title}>
                                1. Información que proporciona el usuario
                                {'\n'}
                            </Text>
                            Puede incluir:
                            {'\n'}
                            <Text style={styles.title}>
                                ▪ Perfil de usuario:
                            </Text>
                            {'\b'}Recopilamos información cuando el usuario crea o actualiza su cuenta
                            de Econosuper. Esta información puede incluir su nombre, email, número de
                            teléfono, nombre de inicio de sesión y contraseña, dirección, información
                            bancaria o de pago (incluida la información de verificación de pago
                            relacionada), y foto. Además, incluye las preferencias y configuraciones
                            seleccionadas en la cuenta de Econosuper.
                            {'\n'}
                            <Text style={styles.title}>
                                ▪ Datos demográficos:
                            </Text>
                            {'\b'}Podemos recopilar información demográfica del usuario mediante
                            métodos como encuestas.
                            {'\n'}
                            <Text style={styles.title}>
                                ▪ Contenido de usuarios:
                            </Text>
                            {'\b'}Podemos recopilar información que envíe cuando se ponga en contacto
                            con el servicio de asistencia al cliente de Econosuper, al proporcionar
                            valoraciones.
                            {'\n'}{'\n'}
                            <Text style={styles.title}>
                                2. Información que se crea al utilizar nuestros servicios                             {'\n'}
                            </Text>
                            Puede incluir:
                            {'\n'}
                            <Text style={styles.title}>
                                ▪ Información sobre la ubicación
                                {'\n'}
                            </Text>
                            En función de los servicios de Econosuper que se utilicen y de la
                            configuración de la app o los permisos del dispositivo, podemos recopilar
                            información precisa o aproximada a través de datos como GPS, dirección IP.
                            {'\n'}
                            ▪ Si el usuario ha dado permiso para procesar sus datos de ubicación,
                            Econosuper recopila información de ubicación cuando la app Econosuper está
                            funcionando en primer plano. En algunas zonas, Econosuper también recopila
                            dicha información cuando la app Econosuper está funcionando en segundo
                            plano si el usuario lo ha permitido en los ajustes de la app o en los
                            permisos del dispositivo.
                            {'\n'}
                            ▪ Si lo desean, los usuarios y los destinatarios de entregas pueden
                            utilizar la app Econosuper sin permitirle que recopile información de
                            ubicación. Sin embargo, esto puede afectar a la funcionalidad de la app.
                            Por ejemplo, si un usuario no permite que Econosuper recopile su información
                            de ubicación, deberá introducir manualmente su dirección de envío.
                            {'\n'}
                            <Text style={styles.title}>
                                ▪ Información sobre transacciones
                                {'\n'}
                            </Text>
                            Recopilamos datos de las transacciones relacionadas con el uso que se
                            haga de los servicios, entre los que se incluyen la información sobre el
                            tipo de servicios que se solicitan o se proporcionan, los detalles del
                            pedido, la información de entrega, la fecha y hora en la que se realizó el
                            servicio, la cantidad que se cobró, la distancia recorrida y la forma de
                            pago. Además, si un usuario utiliza el código promocional de otro,
                            asociaremos ambos nombres entre sí.
                            {'\n'}
                            <Text style={styles.title}>
                                ▪ Información de uso
                                {'\n'}
                            </Text>
                            Recopilamos información sobre la interacción de los usuarios con nuestros
                            servicios. Esta información incluye, por ejemplo, las fechas y las horas de
                            acceso, las características de las apps o las páginas visualizadas, el
                            bloqueo de la app u otra actividad del sistema, el tipo de navegador y los
                            sitios o servicios de terceros que se estuvieran utilizando antes de
                            interactuar con nuestros servicios. En algunos casos, recopilamos esta
                            información mediante cookies, etiquetas de píxel y otras tecnologías
                            similares que crean y mantienen identificadores única.
                            {'\n'}
                            <Text style={styles.title}>
                                ▪ Información del dispositivo
                                {'\n'}
                            </Text>
                            Es posible que recopilemos información sobre los dispositivos que se
                            utilicen para acceder a nuestros servicios como, por ejemplo, el modelo de
                            hardware, la dirección IP, el sistema operativo y la versión del mismo,
                            software, nombres y versiones de archivos, idiomas preferidos,
                            identificadores de dispositivos, identificadores de publicidad, números de
                            serie, información del movimiento del dispositivo e información sobre la red
                            móvil.
                            {'\n'}
                            <Text style={styles.title}>
                                ▪ Datos de comunicaciones
                                {'\n'}
                            </Text>
                            Permitimos que los usuarios se comuniquen con Econosuper a través de
                            las apps, los sitios web y otros servicios de Econosuper. Es posible que
                            Econosuper también utilice esta información para motivos relacionados con
                            su servicio de asistencia al cliente (esto incluye, por ejemplo, la resolución
                            de disputas entre usuarios), para protección y seguridad, para mejorar
                            nuestros productos y servicios y para la realización de análisis.
                            {'\n'}{'\n'}
                            <Text style={styles.title}>
                                3. Información procedente de otras fuentes
                                {'\n'}
                            </Text>
                            Entre esta información, se incluyen:
                            {'\n'}
                            ▪ Comentarios de usuarios, como valoraciones y felicitaciones.
                            {'\n'}
                            ▪ Usuarios que solicitan servicios para usted o en su nombre.
                            {'\n'}
                            ▪ Usuarios u otras entidades que proporcionan información relativa a
                            reclamaciones o litigios.
                            {'\n'}
                            ▪ Socios comerciales de Econosuper a través de los cuales ha creado su
                            cuenta de Econosuper o accede a ella, como proveedores de pago, redes
                            sociales, servicios de música a la carta o apps y sitios web que usan las
                            API de Econosuper o cuyas API usa Econosuper (p. ej., guardar una dirección
                            de Google Maps).
                            {'\n'}
                            ▪ Proveedores de servicios financieros.
                            {'\n'}
                            ▪ Propietarios de un perfil familiar de Econosuper o de un perfil de
                            Econosuper para empresas que utilice.
                            {'\n'}
                            ▪ Proveedores de servicios de marketing.
                            {'\n'}
                            Econosuper puede combinar la información recopilada de estas fuentes con
                            otra información que obre en su poder.
                            {'\n'}{'\n'}
                            <Text style={styles.title}>
                                Como utilizamos tu Información
                                {'\n'}
                            </Text>
                            Econosuper utiliza la información que recopila con distintos fines:
                            {'\n'}{'\n'}
                            <Text style={styles.title}>
                                1. Ofrecer servicios y funciones
                                {'\n'}
                            </Text>
                            Econosuper utiliza la información que recopila para ofrecer, personalizar,
                            mantener y mejorar sus productos y servicios, así como realizar las
                            siguientes actividades:
                            {'\n'}
                            ▪ Crear y actualizar su cuenta.
                            {'\n'}
                            ▪ Verificar su identidad.
                            {'\n'}
                            ▪ Procesar o facilitar el pago de los servicios.
                            {'\n'}
                            ▪ Controlar el progreso de su pedido.
                            {'\n'}
                            ▪ Activar funciones que le permiten compartir información con otras personas,
                            como enviar una felicitación a un repartidor, recomendar un amigo a
                            Econosuper.
                            {'\n'}
                            ▪ Activar funciones para personalizar su cuenta de Econosuper, como crear
                            marcadores de sus productos favoritos y facilitar el acceso a compras
                            anteriores.
                            {'\n'}
                            ▪ Llevar a cabo operaciones internas necesarias para ofrecer nuestros
                            servicios, como corregir errores de software y problemas operativos,
                            realizar análisis de datos, pruebas y estudios o supervisar y analizar las
                            tendencias de uso y actividad.
                            {'\n'}{'\n'}
                            <Text style={styles.title}>
                                2. Protección y seguridad
                                {'\n'}
                            </Text>
                            Utilizamos sus datos para mantener la protección, seguridad e integridad de
                            nuestros servicios y usuarios. Para ello, realizamos actividades como:
                            {'\n'}
                            ▪ Seleccionar a los repartidores antes de permitirles usar nuestros
                            servicios e investigarlos de forma periódica mediante la comprobación de
                            antecedentes (si la ley lo permite) para evitar la participación de
                            repartidores poco seguros.
                            {'\n'}{'\n'}
                            <Text style={styles.title}>
                                3. Asistencia al cliente
                                {'\n'}
                            </Text>
                            Econosuper utiliza la información que recopila para ayudarle cuando
                            contacte con nuestro servicio de asistencia y con los siguientes fines:
                            {'\n'}
                            ▪ Dirigir sus preguntas al empleado más adecuado del servicio de asistencia
                            al cliente.
                            {'\n'}
                            ▪ Investigar y resolver sus dudas.
                            {'\n'}
                            ▪ Supervisar y mejorar las respuestas de nuestro servicio de asistencia al
                            cliente.
                            {'\n'}{'\n'}
                            <Text style={styles.title}>
                                4. Investigación y desarrollo
                                {'\n'}
                            </Text>
                            Podemos utilizar la información que recopilamos para probar, investigar,
                            analizar y desarrollar productos. Esto nos permite mejorar y reforzar la
                            seguridad denuestros servicios, desarrollar nuevas funciones y productos,
                            así como facilitar soluciones de seguros y financiación relacionadas con
                            nuestros servicios.
                            {'\n'}{'\n'}
                            <Text style={styles.title}>
                                5. Comunicaciones entre usuarios
                                {'\n'}
                            </Text>
                            Econosuper utiliza la información que recopila para permitir las
                            comunicaciones entre los usuarios. Por ejemplo, un repartidor puede enviar
                            un mensaje de texto o el repartidor puede llamar al destinatario de una
                            entrega para facilitarle información sobre el pedido.
                            {'\n'}{'\n'}
                            <Text style={styles.title}>
                                Cookies y Tecnologías de Terceros
                                {'\n'}
                            </Text>
                            Las cookies son pequeños archivos de texto que los sitios web, las apps,
                            los medios de comunicación digitales y los anuncios almacenan en su
                            navegador o dispositivo.
                            <Text style={styles.title}>
                                {'\b'}Econosuper utiliza cookies y tecnologías similares con los siguientes fines:
                                {'\n'}
                            </Text>
                            ▪ Autenticación de usuarios.
                            {'\n'}
                            ▪ Recuperación de preferencias y ajustes de usuarios.
                            {'\n'}
                            ▪ Determinación de la popularidad del contenido.
                            {'\n'}
                            ▪ Cálculo y medición de efectividad de campañas publicitarias.
                            {'\n'}
                            ▪ Análisis de tráfico y tendencias del sitio web, así como análisis general
                            de los comportamientos en línea e intereses de los usuarios de nuestros
                            servicios.
                            {'\n'}{'\n'}
                            <Text style={styles.title}>
                                Intercambio de Información y Divulgación
                                {'\n'}
                            </Text>
                            Econosuper puede compartir la información que recopila de las siguientes
                            formas:
                            {'\n'}{'\n'}
                            <Text style={styles.title}>
                                1. Con otros usuarios
                                {'\n'}
                            </Text>
                            ▪ Por ejemplo, si es un repartidor, podemos compartir su nombre, la
                            valoración media del repartidor otorgada por los usuario y las ubicaciones
                            de destino con los repartidores.
                            {'\n'}{'\n'}
                            <Text style={styles.title}>
                                2. Si lo solicita
                                {'\n'}
                            </Text>
                            Incluye compartir su información con:
                            {'\n'}
                            <Text style={styles.title}>
                                ▪ Otras personas que solicite.
                            </Text>
                            {'\b'}Por ejemplo, podemos compartir su hora
                            estimada de entrega y ubicación con un amigo si lo solicita o la información
                            de su pedido cuando divida el precio con un amigo.
                            {'\n'}{'\n'}
                            <Text style={styles.title}>
                                3. Con filiales de Econosuper
                                {'\n'}
                            </Text>
                            Compartimos datos con nuestras filiales para facilitar y mejorar el
                            procesamiento de nuestros datos sobre servicios y conducta en nuestro
                            nombre.
                            {'\n'}{'\n'}
                            <Text style={styles.title}>
                                4. Con los proveedores de servicios y socios comerciales de
                                Econosuper
                                {'\n'}
                            </Text>
                            Econosuper puede facilitar información a proveedores de servicios o socios
                            comerciales. Por ejemplo, puede incluir:
                            {'\n'}
                            ▪ Procesadores y facilitadores de pagos.
                            {'\n'}
                            ▪ Socios de marketing y proveedores de plataformas de marketing.
                            {'\n'}
                            ▪ Proveedores que ayudan a Econosuper a mejorar la seguridad de sus apps.
                            {'\n'}
                            ▪ Consultores, juristas, contables y otros proveedores de servicios
                            profesionales.
                            {'\n'}{'\n'}
                            <Text style={styles.title}>
                                5. Por motivos legales o en caso de litigio
                                {'\n'}
                            </Text>
                            Econosuper puede compartir su información si piensa que así lo exige la
                            legislación vigente, la normativa, un acuerdo de colaboración, un proceso
                            legal o una solicitud oficial, así como cuando su revelación sea pertinente
                            por razones de seguridad o similares.
                            {'\n'}
                            Esto incluye compartir su información con las autoridades legales, las
                            autoridades gubernamentales, los aeropuertos (si las autoridades
                            aeroportuarias lo requieren como condición para operar en la propiedad del
                            aeropuerto) u otros terceros según sea necesario para hacer cumplir nuestras
                            condiciones de servicio, acuerdos de usuario u otras políticas, para proteger
                            los derechos o la propiedad de Econosuper o los derechos, seguridad o
                            propiedad de terceros, o en el caso de reclamaciones o litigios relacionados
                            con el uso de nuestros servicios. Si utilizas la tarjeta de crédito de otra
                            persona, la ley nos puede exigir que compartamos información con el titular
                            de dicha tarjeta, incluida la información del viaje.
                            {'\n'}
                            Esto también incluye compartir su información con otras personas en relación
                            con, o durante negociaciones de, cualquier fusión, venta de activos de la
                            empresa, consolidación o reestructuración, financiación o adquisición de todo
                            o parte de nuestro negocio por parte de otra empresa.
                            {'\n'}{'\n'}
                            <Text style={styles.title}>
                                6. Con su consentimiento
                                {'\n'}
                            </Text>
                            Econosuper puede compartir su información de otra manera que no sea la
                            descrita en esta política si así se lo notificamos y lo acepta.
                            {'\n'}{'\n'}
                            <Text style={styles.title}>
                                Eliminación y retención de la Información
                                {'\n'}
                            </Text>
                            Econosuper necesita información de los perfiles de usuario para proporcionar
                            sus servicios y la conserva durante el tiempo que usted mantiene su cuenta
                            de Econosuper.
                            {'\n'}
                            Econosuper conserva determinadamente la información, entre la que se incluye
                            información de transacciones, ubicación, dispositivo y uso, durante un mínimo de 7años en relación con normativas, impuestos, seguros o cualquier otro requisito que
                            deba cumplir en los lugares en los que opera. Una vez que dicha información ya
                            no sea necesaria para proporcionar los servicios de Econosuper, ofrecer
                            asistencia al cliente, mejorar la experiencia de usuario u otros fines operativos,
                            Econosuper tomará medidas para evitar el acceso a esta información o su uso con
                            cualquier fin que no sea el cumplimiento de estos requisitos o por motivos de
                            seguridad, prevención y detección de problemas de seguridad y fraude.
                            {'\n'}
                            Puede solicitar que se elimine su cuenta en cualquier momento acercándose a
                            Soporte Técnico. De esta manera tras su solicitud, Econosuper elimina la
                            información que no se debe conservar.
                            {'\n'}
                            En determinadas circunstancias, Econosuper no podrá eliminar su cuenta, por
                            ejemplo, si aún queda crédito o hay reclamaciones o disputas sin resolver. Una
                            vez resuelto el problema, Econosuper eliminará su cuenta como se ha descrito
                            anteriormente.
                            {'\n'}
                            Econosuper también podrá conservar determinada información si es necesaria
                            para sus intereses comerciales legítimos, como prevención del fraude, mejora de
                            la seguridad y protección de los usuarios. Por ejemplo, si Econosuper cierra la
                            cuenta de un usuario por un comportamiento peligroso o incidentes relacionados
                            con la seguridad, podrá conservar cierta información sobre la cuenta para evitar
                            que el mismo usuario abra otra en el futuro.
                            {'\n'}{'\n'}
                            <Text style={styles.title}>
                                ELECCIÓN Y TRANSPARENCIA
                                {'\n'}
                                A. CONFIGURACIÓN DE PRIVACIDAD
                                {'\n'}
                            </Text>
                            A continuación se incluye información sobre esta configuración y se describe cómo
                            se establecen o modifican estos ajustes, además del efecto que produce su
                            desactivación.
                            {'\n'}{'\n'}
                            <Text style={styles.title}>
                                ▪ Información sobre la ubicación
                                {'\n'}
                            </Text>
                            {'\b'}{'\b'}▪ Econosuper usa los servicios de ubicación del dispositivo del usuario
                            para que disfrute de viajes seguros y fiables siempre que los
                            necesite. Los datos de ubicación nos ayudan a mejorar nuestros
                            servicios, que incluyen las recogidas, la navegación y la asistencia al
                            cliente.
                            {'\n'}
                            {'\b'}{'\b'}▪ Si desactiva los servicios de ubicación del dispositivo, afectará al uso
                            de la app Econosuper. Por ejemplo, deberá introducir manualmente
                            sus ubicaciones de entrega.
                            {'\n'}{'\n'}
                            <Text style={styles.title}>
                                ▪ Notificaciones: Actualizaciones de la cuenta y las entregas
                                {'\n'}
                            </Text>
                            {'\b'}{'\b'}▪ Econosuper envía a los usuarios notificaciones y actualizaciones del
                            estado del pedido relacionadas con su cuenta. Estas notificacionesson una parte necesaria del uso de la app Econosuper y no se
                            pueden desactivar. Sin embargo, puede elegir el método a través del
                            que desea recibir estas notificaciones mediante el menú
                            Configuración de privacidad.
                            {'\n'}{'\n'}
                            <Text style={styles.title}>
                                ▪ Notificaciones: Descuentos y noticias
                                {'\n'}
                            </Text>
                            {'\b'}{'\b'}▪ Puede permitir que Econosuper le envíe notificaciones sobre
                            descuentos y noticias de Econosuper. Puede activar y desactivar
                            estas notificaciones en cualquier momento mediante el menú
                            Configuración de privacidad de la app Econosuper.
                            {'\n'}{'\n'}
                            <Text style={styles.title}>
                                B. PERMISOS DEL DISPOSITIVO
                                {'\n'}
                            </Text>
                            La mayoría de plataformas móviles (iOS, Android, etc.) tienen definidos ciertos
                            tipos de datos del dispositivo a los que las apps no pueden acceder sin que dé su
                            consentimiento. Dichas plataformas tienen distintos sistemas de autorización para
                            obtener su consentimiento. La plataforma iOS le avisará la primera vez que la app
                            Econosuper quiera obtener permiso para acceder a cierto tipo de datos y le
                            permitirá que conceda, o no, su consentimiento a dicha solicitud. Los dispositivos
                            Android le notificarán los permisos que la app Econosuper requiere antes de que
                            usted la utilice por primera vez. Su uso constituirá su consentimiento.
                            {'\n'}{'\n'}
                            <Text style={styles.title}>
                                C. CONSULTA DE VALORACIONES
                                {'\n'}
                            </Text>
                            Después de cada pedido, los repartidores y los usuarios pueden valorarse entre sí
                            y comentar qué tal ha sido el pedido. Este sistema bidireccional hace que todo el
                            mundo se haga responsable de su comportamiento. El hecho de que todo el
                            mundo deba asumir su parte de responsabilidad crea un entorno seguro y
                            respetuoso tanto para los repartidores como para los usuarios.
                            {'\n'}
                            La valoración del usuario esta aparece cuando el repartidor va a aceptar el pedido.
                            La valoración de los repartidores está disponible cuando se va a realizar el pedido.
                            {'\n'}{'\n'}
                            <Text style={styles.title}>
                                D. ACLARACIONES, COPIAS Y CORRECCIÓN
                                {'\n'}
                            </Text>
                            Puede hacer lo siguiente:
                            {'\n'}
                            ▪ Solicitar que Econosuper proporcione una explicación detallada de la
                            información que ha recopilado sobre usted y cómo la utiliza.
                            {'\n'}
                            ▪ Solicitar una copia de la información que Econosuper ha recopilado sobre
                            usted.
                            {'\n'}
                            ▪ Solicitar la corrección de cualquier información imprecisa sobre usted que
                            tenga Econosuper.
                            {'\n'}
                            Es posible también editar el nombre, el número de teléfono y la dirección de
                            email asociados a su cuenta. Asimismo, puede consultar su historial de
                            viajes, sus ganancias, información personal, documentación del vehículo.
                            {'\n'}{'\n'}
                            <Text style={styles.title}>
                                ACTUALIZACIONES A ESTA POLITICA
                                {'\n'}
                            </Text>
                            Es posible que actualicemos esta política de forma ocasional. Si llevamos a cabo
                            cambios significativos, te los notificaremos a través de las aplicaciones de
                            Econosuper o a través de otro medio, como por email. Conforme a lo establecido
                            por la ley aplicable, al usar nuestros servicios tras haber recibido dicha
                            notificación, estarás aceptando las actualizaciones de la política.
                            {'\n'}
                            Te recomendamos que revises de forma periódica la presente política para obtener
                            la información más actualizada sobre nuestras prácticas de privacidad. Si lo
                            deseas, también pondremos a tu disposición las versiones previas de nuestras
                            políticas de privacidad para que puedas consultarlas.
                        </Text>
                    </ScrollView>
                </TabView.Item>
            </TabView>
        </View>
    )
}

export default TerminosCondiciones

const styles = StyleSheet.create({
    container: {
        width: "100%",
        height: "100%",
        backgroundColor: "#fff"
    },
    text: {
        fontSize: 18,
        textAlign: 'justify',
        lineHeight: 25
    },
    title: {
        fontSize: 18,
        textAlign: 'justify',
        fontWeight: 'bold',
    },
    tab: {
        width: '100%',
        height: '100%',
        paddingVertical: 30,
        paddingHorizontal: 25
    },
    activeTitle: {
        color: "#FE430E"
    },
    inactiveTitle: {
        color: "black"
    }
})