import React from 'react';
import { StyleSheet, Text, View, ActivityIndicator } from 'react-native';
import {Input,Button,Icon,Avatar,Image} from 'react-native-elements';
import {useNavigation} from '@react-navigation/native'

const Invitado = () => {
    const navegacion = useNavigation();
    return (
        <View style={{backgroundColor:"#fff",height:"100%",justifyContent:"center"}} >
            <View style={{justifyContent:"center",alignItems:"center"}} >
                <Image
                    source={require("../../../assets/images/econoLogo.png")}
                    style={{  width: 264,
                        height: 106 }}
                    resizeMode ="cover"
                    PlaceholderContent={<ActivityIndicator />}
                />
            </View>
            <View style={{justifyContent:"center",alignItems:"center",marginTop:"25%"}} >
                <Button
                    title="Iniciar sesión"
                    containerStyle={styles.containerBtnInicioSesion}
                    buttonStyle = {styles.btnInicioSesion}
                    onPress = {()=>{navegacion.navigate("login")}}
                />
                <Button
                    title="Registrarte"
                    containerStyle={styles.containerBtnRegistrarte}
                    buttonStyle = {styles.btnRegistrarte}
                    onPress = {()=>{navegacion.navigate("ingresaNumero")}}
                />
            </View>
        </View>
    )
}

export default Invitado

const styles = StyleSheet.create({
    btnInicioSesion:{
        borderRadius: 6,
        backgroundColor: "rgba(232,103,36,1)",
    },
    containerBtnInicioSesion:{
        width:"90%",
        height:50
    },
    containerBtnRegistrarte:{
        width:"90%",
        height:50 
    },
    btnRegistrarte:{
        borderRadius: 6,
        backgroundColor: "rgba(39,87,153,1)",
    }
})
