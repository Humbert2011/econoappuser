import React, { useState, useEffect } from 'react'
import { StyleSheet, View, ActivityIndicator } from 'react-native'
import { Image, Icon, Button, Text, Tab, TabView } from 'react-native-elements';

import ToastAddCart from '../../components/cart/ToastAddCart';
import { addElementCart } from '../../utils/cartStorage';

const DetalleProd = ({ route }) => {
    const { urlImg, nombre, precio, descripcion, informacion_adicional, embalaje } = route.params;
    const [index, setIndex] = useState(0);
    const [total, setTotal] = useState(precio);
    const [number, setNumber] = useState(1);
    const [showToast, setShowToast] = useState(false);
    const [loaderButton, setLoaderButton] = useState(false);

    useEffect(() => {
        let n = (number * precio);
        setTotal(n.toFixed(2));
    }, [number])

    const addCart = async () => {
        setLoaderButton(true);
        let add = addElementCart(route.params, number);
        await setTimeout(() => {
            setLoaderButton(false);
        }, 1000)
        add && setShowToast(true);
    }

    const sumaProducto = () => {
        let num = 1
        embalaje == 'granel' && (num = 0.25);
        setNumber(number+(number  < 99 ? num : 0))

    }

    const restaProducto = () => {
        let num = 1
        embalaje == 'granel' && (num = 0.25);
        (number != num) && setNumber(number - num)
    }

    return (
        <View style={{ width: "100%", height: "100%", backgroundColor: "#fff" }} >
            <View style={{ alignItems: "center", justifyContent: "center", marginTop: 10, marginBottom: 15 }}>
                <Image
                    resizeMode="cover"
                    source={urlImg ? { uri: urlImg } : require('../../../assets/no-image.png')}
                    PlaceholderContent={<ActivityIndicator size="large" color="#3D5CA4" />}
                    placeholderStyle={{ backgroundColor: '#fff' }}
                    style={{
                        height: 210,
                        width: 210
                    }}
                />
            </View>
            <View style={{ alignItems: "center", justifyContent: "center" }}><Icon name="circle" size={10} color="rgba(61,92,164,1)" /></View>
            <View style={{ margin: 20 }}>
                <Text style={{ fontSize: 15 }} >{nombre}</Text>
                <Text style={{ fontWeight: 'bold', fontSize: 20 }}>${precio}</Text>
            </View>

            <View style={{ margin: 20, flexDirection: "row" }}>
                <View style={styles.viewSumar}>
                    <View style={{ width: '30%' }}>
                        <Icon
                            // containerStyle = {{paddingRight:'25%'}}
                            size={17}
                            name='minus'
                            type='font-awesome'
                            color='#3D5CA4'
                            onPress={() => restaProducto()}
                        />
                    </View>
                    <View style={{ width: '40%', alignItems: "center" }}>
                        <Text style={{ fontSize: 18, color: 'black', fontWeight: "bold" }}>{number}</Text>
                    </View>
                    <View style={{ width: '30%' }}>
                        <Icon
                            // containerStyle = {{paddingLeft:'25%'}}
                            size={17}
                            name='plus'
                            type='font-awesome'
                            color='#3D5CA4'
                            onPress={() => sumaProducto()}
                        />
                    </View>
                </View>
                <Button
                    loadingProps={{ color: '#3D5CA4' }}
                    loading={loaderButton}
                    loadingStyle={styles.loaderStyle}
                    buttonStyle={!loaderButton ? styles.btnAgregarActivo : styles.btnAgregarLoader}
                    containerStyle={styles.containerAgregar}
                    title={`Agregar  $${total}`}
                    onPress={() => addCart()}
                />
            </View>
            <Tab value={index} onChange={setIndex} indicatorStyle={{ backgroundColor: "#3D5CA4" }}>
                <Tab.Item containerStyle={{ backgroundColor: "#fff" }} titleStyle={{ color: "black", fontSize: 17, fontWeight: 'bold', textTransform: 'capitalize' }} title="Descripción" />
                <Tab.Item containerStyle={{ backgroundColor: "#fff" }} titleStyle={{ color: "black", fontSize: 17, fontWeight: 'bold', textTransform: 'capitalize' }} title="Características" />
            </Tab>
            <TabView value={index} onChange={setIndex} >
                <TabView.Item style={{ width: '100%', height: '100%' }}>
                    <Text style={{ fontSize: 16, margin: 10 }}>{descripcion}</Text>
                </TabView.Item>
                <TabView.Item style={{ width: '100%', height: '100%' }}>
                    <Text style={{ fontSize: 16, margin: 10 }}>{informacion_adicional}</Text>
                </TabView.Item>
            </TabView>
            <ToastAddCart showToast={showToast} setShowToast={setShowToast} />
        </View>
    )
}

export default DetalleProd

const styles = StyleSheet.create({
    viewSumar: {
        width: "40%",
        height: 45,
        borderRadius: 7,
        borderWidth: 1,
        borderColor: "#BEBEBE",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        padding: 2,
        marginRight: "5%"
    },
    btnAgregarActivo: {
        borderRadius: 7,
        height: 45,
        backgroundColor: "#3D5CA4"
    },
    btnAgregarLoader: {
        borderRadius: 7,
        height: 45,
        backgroundColor: "#fff",
        borderColor: "#3D5CA4",
        borderWidth: 2,
    },
    containerAgregar: {
        width: "55%",
        height: 45,
    }
})
