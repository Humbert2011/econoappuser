import React, { useState, useCallback } from 'react'
import { StyleSheet, Text, View, ScrollView, TouchableOpacity, ActivityIndicator } from 'react-native'
import { ListItem, Icon, Divider } from 'react-native-elements';
import { useNavigation, useFocusEffect } from '@react-navigation/native';

import { getValueFor } from '../../utils/localStorage';
import Api from '../../utils/Api';
import { url } from '../../utils/config';

import ListaMetodosPago from '../../components/metodoPago/ListaMetodosPago';
import MetodoDefault from '../../components/metodoPago/MetodoDefault';
const MetodoPago = () => {
    const navegacion = useNavigation();
    const [reload, setReload] = useState(false);
    const [metodosPago, setMetodosPago] = useState(null);
    const [metodoDefault, setMetodoDefault] = useState(null);
    const [token, setToken] = useState(null);
    const [idUser, setidUser] = useState(null);

    useFocusEffect(
        useCallback(() => {
            (async () => {

                let token = null;
                let id = null;
                await getValueFor("token").then(res => {
                    token = res;
                    setToken(res);
                });
                await getValueFor("id").then(res => {
                    id = res;
                    setidUser(id);
                });
                let uri = `${url}metodopago/tolist/${id}`;
                let api = new Api(uri, "GET", null, token);
                api.call().then(res => {
                    if (res.response) {
                        // setMetodoDefault(obtenerMetodoDefault(res.result));
                        const metodoDefault = obtenerMetodoDefault(res.result, id);
                        metodoDefault && setMetodoDefault(metodoDefault);
                        setMetodosPago(res.result)

                    } else {
                        console.log(res.result);
                        (res.result == 401) && navegacion.navigate("espera");
                    }
                });
                setReload(false)
            })()
        }, [reload])
    )

    const cambiarMetodoPago = async (metodopago) => {
        let uri = `${url}metodopago/default/${idUser}`;
        let parametros = {
            metodopago: metodopago
        }
        let api = new Api(uri, "PUT", parametros, token);
        await api.call().then(res => {
            if (res.response) {
                setMetodosPago(null);
                setMetodoDefault(null);
                setReload(true);
            } else {
                alert("Ocurrio un error al intentar actualizar el metodo de pago")
            }
        });
    };

    return (
        <ScrollView style={{ backgroundColor: "#efeff4", marginTop: 30 }} >
            <Divider orientation='horizontal' />
            {
                metodoDefault ? <MetodoDefault metodoDefault={metodoDefault} /> : <ActivityIndicator style={{ marginTop: 30 }} size="large" color="#FF6719" />
            }
            <Divider orientation='horizontal' />
            {/* <Text style={styles.tituloSeleccione} >Seleccione otro método de pago</Text>
            {
               
                metodosPago ? <ListaMetodosPago metodosPago = {metodosPago} cambiarMetodoPago = {cambiarMetodoPago}/> :  <ActivityIndicator style = {{marginTop:30}} size="large" color="#FF6719" />
            }

                <View>
                <TouchableOpacity style={{alignItems:"center",justifyContent:"center"}} onPress = { () => console.log("Agregar Tarjetta ...") }  >
                    <Text style ={{fontSize:26}} >+</Text>
                    <Text style={{fontSize:16}} >Agregar</Text>
                </TouchableOpacity>
            </View> */}
        </ScrollView>
    )
}

const obtenerMetodoDefault = (data, id) => {
    return data.find(elemento => elemento.idpersona == id);
};

export default MetodoPago

const styles = StyleSheet.create({
    tituloSeleccione: {
        fontSize: 16,
        marginTop: 30,
        marginLeft: 5
    },
    agregarTar: {
        backgroundColor: "#fff",
    }
})
