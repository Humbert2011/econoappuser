import { StyleSheet, View,ActivityIndicator,Dimensions,ScrollView} from 'react-native';
import React,{useState,useCallback} from 'react';
import {Image,Text} from 'react-native-elements';
import { useFocusEffect } from '@react-navigation/native';
import {map} from 'lodash';

import Api from '../../utils/Api';
import {url} from '../../utils/config';
import {getValueFor,saveValue} from '../../utils/localStorage';
import ProductCard from '../../components/ProductCard';

const Home = () => {
  const [productos, setProductos] = useState(null);
  const [ofertas, setOfertas] = useState(null);
  useFocusEffect(
    useCallback(() => {
        (async()=>{
            let token= null;
            await getValueFor("token").then(res=>token = res);
            let uri = '';
            let api = null;

            uri = `${url}config/`;
            api = new Api(uri,"GET");
            await api.call().then(res => {
                if (res.response) {
                    let vars = res.result;
                    vars.forEach(async(element) => {
                        await saveValue(element.variable, element.valor)
                    });
                }else{
                    (res.result == 401) && navegacion.navigate("cuenta");
                }
            });

            uri = `${url}product/sugerenciasProducto`;
            api = new Api(uri,"GET");
            api.call().then(res => {
                if (res.response) {
                  setProductos(res.result.Productos)
                }else{
                    (res.result == 401) && navegacion.navigate("cuenta");
                }
            });

            uri = `${url}product/homeofertaProducto`;
            api = new Api(uri,"GET");
            api.call().then(res => {
                if (res.response) {
                  setOfertas(res.result.Ofertas)
                }else{
                    (res.result == 401) && navegacion.navigate("cuenta");
                }
            });
        })()
    },[])
  )
  return (
    <ScrollView style={{backgroundColor : "#fff", height:"100%",width:"100%"}} >
        <View style={{backgroundColor : "#fff",width:'100%',height:190,alignItems:"center" }}>
          <Image
              resizeMode = "stretch"
              PlaceholderContent={<ActivityIndicator size='small' color="#3D5CA4"/>}
              placeholderStyle = {{backgroundColor: '#fff'}}
              source = {
                 require("../../../assets/fv1.png")
              }
              style = {{width:360, height: 180, marginRight:2,borderRadius:20,marginTop:5}}
          />
        </View>
        <View style={{backgroundColor : "#fff",width:'100%',height:'70%',margin:15}}>
          <Text h4>Ofertas de la semana.</Text>
          <View style={{backgroundColor:"#fff",justifyContent:"center"}}>
              <View style={{height:245}} >
                {
                  !ofertas ? (
                      <View style={{width:'100%',height:'100%',justifyContent:'center'}} >
                          <ActivityIndicator size="large" color="black"/>
                      </View>
                  ) : (
                      <ScrollView horizontal = {true}>
                          {
                              map(ofertas, (producto) => (
                                  <ProductCard key={producto.idproducto} producto={producto}/>
                              ))
                          }
                      </ScrollView>
                  )
                }
              </View>
          </View>
          <Text h4>Recomendaciones</Text>
          <View style={{backgroundColor:"#fff",justifyContent:"center"}}>
          <View style={{height:245}} >
              {
                  !productos ? (
                      <View style={{width:'100%',height:'100%',justifyContent:'center'}}>
                          <ActivityIndicator size="large" color="black"/>
                      </View>
                  ) : (
                      <ScrollView horizontal = {true}>
                          {
                              map(productos, (producto) => (
                                  <ProductCard key={producto.idproducto} producto={producto}/>
                              ))
                          }
                      </ScrollView>
                  )
                }
              </View>
          </View>
        </View>
    </ScrollView>
  );
};

export default Home;

const styles = StyleSheet.create({});
