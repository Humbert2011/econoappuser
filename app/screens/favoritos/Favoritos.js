import React,{useState,useCallback} from 'react'
import { StyleSheet, Text, View, FlatList } from 'react-native'
import { useFocusEffect,useNavigation } from '@react-navigation/native';
import {size} from 'lodash';

import ProductCard from '../../components/ProductCard';
import Api from '../../utils/Api';
import {url} from '../../utils/config';
import {getValueFor} from '../../utils/localStorage';

const Favoritos = () => {
    const navegacion = useNavigation();
    const [token, settoken] = useState(null);
    const [productos, setproductos] = useState(null);
    
    useFocusEffect(
        useCallback(() => {
            (async()=>{
                let token= null;
                let id = null;
                await getValueFor("token").then(res=>{
                    token = res
                    settoken(token)
                });
                if (token){
                    await getValueFor("id").then(res=> id = res);
                    let uri = `${url}favoritos/listfavidpers/${id}`;
                    let api = new Api(uri,"GET",null,token);
                    await api.call().then(res=> {
                        if (res.response) {
                            setproductos(res.result)
                        }
                    })
                }
            })()
        },[])
    )

    return (
        <View style={{padding:10,paddingBottom:50,backgroundColor:'#fff',height:'100%'}}>
            {
                !token ? (
                    <Text style = {{color:'grey'}} onPress = { () => navegacion.navigate("cuenta") } >Inicie session para ver esta seccion!!</Text>    
                ) : (
                    size(productos) > 0 ? (
                        <View>
                            <Text style={{fontSize:19,fontWeight:'bold',marginBottom:20}}>Mis favoritos</Text>
                            <FlatList
                                horizontal = {false}
                                numColumns = {2}
                                data = {productos}
                                renderItem = { ( producto ) => <Producto producto = {producto}/>  }
                                keyExtractor = { (item, index) => index.toString()}
                            />
                        </View>
                    ):(
                        <Text style = {{color:'grey'}}>La lista de favoritos esta en blanco</Text>
                    )
                )
            }
        </View>
        
    )
}
const Producto = ({producto}) => {
    const {item} = producto;
    return(
        <View style ={{width:'50%'}}>
            <ProductCard producto = {item}/>
        </View>
    )
}
export default Favoritos

const styles = StyleSheet.create({})
