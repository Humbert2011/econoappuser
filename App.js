import React,{useEffect,useState,useRef} from 'react';
// solucionar warnig
import {LogBox} from 'react-native';

import {encode,decode} from 'base-64'; //sistema de base 64

import Navigation from "./app/navigation/Navigation";
// imports de notificacciones
import * as Device from 'expo-device';
import * as Notifications from 'expo-notifications';
import {saveValue} from './app/utils/localStorage';

// VirtualizedLists
LogBox.ignoreLogs(["Setting a timer"]);
LogBox.ignoreLogs(["VirtualizedLists"]);
LogBox.ignoreLogs(["Possible Unhandle Promise"]);
LogBox.ignoreLogs(["Encountered two children with the same key"]);
if (!global.btoa) global.btoa = encode;
if (!global.atob) global.atob = decode; 

// push
Notifications.setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: true,
    shouldPlaySound: false,
    shouldSetBadge: false,
  }),
});

export default function App() {
  // obtener token y funcionde obtencion de push
  // const [expoPushToken, setExpoPushToken] = useState('');
  const [notification, setNotification] = useState(false);
  const notificationListener = useRef();
  const responseListener = useRef();
  useEffect(() => {
    registerForPushNotificationsAsync().then(token => {
      token && saveValue('tokenPush',token)
    });
    notificationListener.current = Notifications.addNotificationReceivedListener(notification => {
      setNotification(notification);
    });

    responseListener.current = Notifications.addNotificationResponseReceivedListener(response => {
      console.log(response);
    });

    return () => {
      Notifications.removeNotificationSubscription(notificationListener.current);
      Notifications.removeNotificationSubscription(responseListener.current);
    };
  }, []);
  return (
    <Navigation/>
  );
}

async function registerForPushNotificationsAsync() {
  let token;
  if (Device.isDevice) {
    const { status: existingStatus } = await Notifications.getPermissionsAsync();
    let finalStatus = existingStatus;
    if (existingStatus !== 'granted') {
      const { status } = await Notifications.requestPermissionsAsync();
      finalStatus = status;
    }
    if (finalStatus !== 'granted') {
      alert('Failed to get push token for push notification!');
      return;
    }
    token = (await Notifications.getExpoPushTokenAsync()).data;
    console.log(token);
    // alert(token)
  } else {
    alert('Must use physical device for Push Notifications');
  }

  if (Platform.OS === 'android') {
    Notifications.setNotificationChannelAsync('default', {
      name: 'default',
      importance: Notifications.AndroidImportance.MAX,
      vibrationPattern: [0, 250, 250, 250],
      lightColor: '#FF231F7C',
    });
  }

  return token;
}
